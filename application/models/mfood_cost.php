<?php
	class Mfood_cost extends CI_Model {

		function __construct() {
			// Call the Model constructor
			parent:: __construct();
			$this->db = $this->load->database('ballyhoo', TRUE);
			//set waktu yang digunakan ke zona jakarta
			//$this->db->query("SET time_zone='Asia/Jakarta'");
		}
		
		public function select($selectcolumn=true)
		{
	     	if($selectcolumn){
		    	$this->db->select('FC_ID');
		    	$this->db->select('FC_NAMA');
		    	$this->db->select('FC_VALUE');
		    	$this->db->select('FC_STATUS');
	        }
            $this->db->from('food_cost');
		}

		function get($where = "", $order = "FC_ID asc", $limit=null, $offset=null, $selectcolumn = true)
		{
  			$this->select($selectcolumn);
  			if($limit != null) $this->db->limit($limit, $offset);
  			if($where != "") $this->db->where($where);
  			$this->db->order_by($order);
  			$query = $this->db->get();
  			return $query->result();
        }
		
		function get_by_id($FC_ID)
		{
			$this->db->where("id", $FC_ID);
			$query = $this->db->get('food_cost');
			return $query->result();
		}

		function save($data)
		{
			$this->db->insert('food_cost', $data);
		}

		function update($FC_ID, $data)
		{
			$this->db->where("FC_ID", $FC_ID);
			$this->db->update('food_cost', $data);
		}

		function delete($FC_ID)
		{
			$data = array();
			$data['FC_STATUS'] = STATUS_DELETE;
			return $this->db->update('food_cost', $data, "FC_ID = $FC_ID");
		}
	}
?>
