<?php
	class Mposisi_karyawan extends CI_Model {

		function __construct() {
			// Call the Model constructor
			parent:: __construct();
			$this->db = $this->load->database('ballyhoo', TRUE);
			// set waktu yang digunakan ke zona jakarta
			//$this->db->query("SET time_zone='Asia/Jakarta'");
		}
		public function select($selectcolumn=true){
	     	if($selectcolumn){
		    	$this->db->select('KP_ID');
		    	$this->db->select('KP_NAMA');
		    	$this->db->select('KP_STATUS');
	        }
            $this->db->from('kry_posisi');
		}

		function get($where = "", $order = "KP_ID asc", $limit=null, $offset=null, $selectcolumn = true){
  			$this->select($selectcolumn);
  			if($limit != null) $this->db->limit($limit, $offset);
  			if($where != "") $this->db->where($where);
  			$this->db->order_by($order);
  			$query = $this->db->get();
  			return $query->result();
        }
		
		function get_by_id($KP_ID)
		{
			$this->db->where("id", $KP_ID);
			$query = $this->db->get('kry_posisi');
			return $query->result();
		}

		function save($data){
			$this->db->insert('kry_posisi', $data);
		}

		function update($KP_ID, $data)
		{
			$this->db->where("KP_ID", $KP_ID);
			$this->db->update('kry_posisi', $data);
		}

		function delete($KP_ID){
			$data = array();
			$data['KP_STATUS'] = STATUS_DELETE;
			return $this->db->update('kry_posisi', $data, "KP_ID = $KP_ID");
		}

		function count_all($where = "")
		{
			if($where != null)$this->db->where($where);
			return $this->db->count_all_results('kry_posisi');
		}
	}
?>
