<?php
	class Mmulti_satuan extends CI_Model {

		function __construct() {
			// Call the Model constructor
			parent:: __construct();
			$this->db = $this->load->database('ballyhoo', TRUE);
			//set waktu yang digunakan ke zona jakarta
			//$this->db->query("SET time_zone='Asia/Jakarta'");
		}
		
		public function select($selectcolumn=true)
		{
	     	if($selectcolumn){
		    	$this->db->select('MS_ID');
		    	$this->db->select('MS_SATUAN1');
		    	$this->db->select('MS_VALUE1');
		    	$this->db->select('u1.ST_NAMA as SATUAN1');
		    	$this->db->select('MS_SATUAN2');
		    	$this->db->select('MS_VALUE2');
		    	$this->db->select('u2.ST_NAMA as SATUAN2');
		    	$this->db->select('MS_STATUS');
	        }
            $this->db->from('multi_satuan');
            $this->db->join('satuan as u1', 'u1.ST_KODE = multi_satuan.MS_SATUAN1');
            $this->db->join('satuan as u2', 'u2.ST_KODE = multi_satuan.MS_SATUAN2');
		}

		public function get_unit($where = "")
		{
            $this->db->where($where);
            $query = $this->db->get('satuan');
  			return $query->result();
		}

		function get($where = "", $order = "MS_ID asc", $limit=null, $offset=null, $selectcolumn = true)
		{
  			$this->select($selectcolumn);
  			if($limit != null) $this->db->limit($limit, $offset);
  			if($where != "") $this->db->where($where);
  			$this->db->group_by('MS_ID');
  			$this->db->order_by($order);
  			$query = $this->db->get();
  			return $query->result();
        }
		
		function get_by_id($MS_ID)
		{
			$this->db->where("id", $MS_ID);
			$query = $this->db->get('multi_satuan');
			return $query->result();
		}

		function save($data)
		{
			$this->db->insert('multi_satuan', $data);
		}

		function update($MS_ID, $data)
		{
			$this->db->where("MS_ID", $MS_ID);
			$this->db->update('multi_satuan', $data);
		}

		function delete($MS_ID)
		{
			$data = array();
			$data['MS_STATUS'] = STATUS_DELETE;
			return $this->db->update('multi_satuan', $data, "MS_ID = $MS_ID");
		}
	}
?>
