<?php
	class Mkaryawan extends CI_Model {

		function __construct() {
			// Call the Model constructor
			parent:: __construct();
			$this->db = $this->load->database('ballyhoo', TRUE);
			//set waktu yang digunakan ke zona jakarta
			//$this->db->query("SET time_zone='Asia/Jakarta'");
		}
		
		public function select($selectcolumn=true)
		{
	     	if($selectcolumn){
		    	$this->db->select('KR_ID');
		    	$this->db->select('p.KP_ID');
		    	$this->db->select('KP_NAMA');
		    	$this->db->select('KR_NAMA');
		    	$this->db->select('KR_ALAMAT');
		    	$this->db->select('KR_TELP');
		    	$this->db->select('KR_USERNAME');
		    	$this->db->select('KR_PASSWORD');
		    	$this->db->select('KR_STATUS');
	        }
            $this->db->from('karyawan as k');
            $this->db->join('kry_posisi as p', 'k.KP_ID = p.KP_ID');
		}

		function get($where = "", $order = "KR_ID asc", $limit=null, $offset=null, $selectcolumn = true)
		{
  			$this->select($selectcolumn);
  			if($limit != null) $this->db->limit($limit, $offset);
  			if($where != "") $this->db->where($where);
  			$this->db->order_by($order);
  			$query = $this->db->get();
  			return $query->result();
        }

        public function get_pos($where = "")
		{
            $this->db->where($where);
            $query = $this->db->get('kry_posisi');
  			return $query->result();
		}
		
		function get_by_id($KR_ID)
		{
			$this->db->where("id", $KR_ID);
			$query = $this->db->get('karyawan');
			return $query->result();
		}

		function save($data)
		{
			$this->db->insert('karyawan', $data);
		}

		function update($KR_ID, $data)
		{
			$this->db->where("KR_ID", $KR_ID);
			$this->db->update('karyawan', $data);
		}

		function delete($KR_ID)
		{
			$data = array();
			$data['KR_STATUS'] = STATUS_DELETE;
			return $this->db->update('karyawan', $data, "KR_ID = $KR_ID");
		}
	}
?>
