<?php
	class Mkat_bahan extends CI_Model {

		function __construct() {
			// Call the Model constructor
			parent:: __construct();
			$this->db = $this->load->database('ballyhoo', TRUE);
			//set waktu yang digunakan ke zona jakarta
			//$this->db->query("SET time_zone='Asia/Jakarta'");
		}
		
		public function select($selectcolumn=true)
		{
	     	if($selectcolumn){
	     		$this->db->select('KB_ID');
	     		$this->db->select('KB_NAMA');
		    	$this->db->select('kb.JB_ID');
		    	$this->db->select('jb.JB_NAMA');
		    	$this->db->select('KB_STATUS');
	        }
	        $this->db->from('kat_bahan as kb');
            $this->db->join('jenis_barang as jb', 'kb.JB_ID = jb.JB_ID');
		}

		public function get_item($where = "")
		{
            $this->db->where($where);
            $query = $this->db->get('jenis_barang');
  			return $query->result();
		}

		function get($where = "", $order = "KB_ID asc", $limit=null, $offset=null, $selectcolumn = true)
		{
  			$this->select($selectcolumn);
  			if($limit != null) $this->db->limit($limit, $offset);
  			if($where != "") $this->db->where($where);
  			$this->db->order_by($order);
  			$query = $this->db->get();
  			return $query->result();
        }
		
		function get_by_id($KB_ID)
		{
			$this->db->where("id", $KB_ID);
			$query = $this->db->get('kat_bahan');
			return $query->result();
		}

		function save($data)
		{
			$this->db->insert('kat_bahan', $data);
		}

		function update($KB_ID, $data)
		{
			$this->db->where("KB_ID", $KB_ID);
			$this->db->update('kat_bahan', $data);
		}

		function delete($KB_ID)
		{
			$data = array();
			$data['KB_STATUS'] = STATUS_DELETE;
			return $this->db->update('kat_bahan', $data, "KB_ID = $KB_ID");
		}
	}
?>
