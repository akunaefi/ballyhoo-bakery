<?php
	class Mlogin extends CI_Model {

		//ambil tabel users

		public function cek_user($username, $password) {
			//cari username dan validsai
			$this->db->where('username', $username);
			$query = $this->db->get('login')->row();

			//user tidak ditemukan
			if(!$query) return 1;

			//user password salah 
			$hash = $query->password;
			if (md5($password) != $hash) return 2;

			return $query;
		}

	}