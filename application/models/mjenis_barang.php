<?php
	class Mjenis_barang extends CI_Model {

		function __construct() {
			// Call the Model constructor
			parent:: __construct();
			$this->db = $this->load->database('ballyhoo', TRUE);
			//set waktu yang digunakan ke zona jakarta
			//$this->db->query("SET time_zone='Asia/Jakarta'");
		}
		
		public function select($selectcolumn=true)
		{
	     	if($selectcolumn){
		    	$this->db->select('JB_ID');
		    	$this->db->select('JB_NAMA');
		    	$this->db->select('JB_STATUS');
	        }
            $this->db->from('jenis_barang');
		}

		function get($where = "", $order = "JB_ID asc", $limit=null, $offset=null, $selectcolumn = true)
		{
  			$this->select($selectcolumn);
  			if($limit != null) $this->db->limit($limit, $offset);
  			if($where != "") $this->db->where($where);
  			$this->db->order_by($order);
  			$query = $this->db->get();
  			return $query->result();
        }
		
		function get_by_id($JB_ID)
		{
			$this->db->where("id", $JB_ID);
			$query = $this->db->get('jenis_barang');
			return $query->result();
		}

		function save($data)
		{
			$this->db->insert('jenis_barang', $data);
		}

		function update($JB_ID, $data)
		{
			$this->db->where("JB_ID", $JB_ID);
			$this->db->update('jenis_barang', $data);
		}

		function delete($JB_ID)
		{
			$data = array();
			$data['JB_STATUS'] = STATUS_DELETE;
			return $this->db->update('jenis_barang', $data, "JB_ID = $JB_ID");
		}
	}
?>
