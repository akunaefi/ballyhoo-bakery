<?php
	class Mbahan extends CI_Model {

		function __construct() {
			// Call the Model constructor
			parent:: __construct();
			$this->db = $this->load->database('ballyhoo', TRUE);
			//set waktu yang digunakan ke zona jakarta
			//$this->db->query("SET time_zone='Asia/Jakarta'");
		}
	
		public function select($selectcolumn=true)
		{
	     	if($selectcolumn){
		    	$this->db->select('BHN_ID');
		    	$this->db->select('BHN_NAMA');
		    	$this->db->select('BHN_HARGA');
		    	$this->db->select('BHN_STOK');
		    	$this->db->select('BHN_MINSTOK');
		    	$this->db->select('BHN_MINORDER');
		    	$this->db->select('BHN_EXP');
		    	$this->db->select('BHN_UPDATE');
		    	$this->db->select('BHN_STATUS');
		    	$this->db->select('b.SP_ID');
		    	$this->db->select('b.KB_ID');
		    	$this->db->select('b.ST_ID');
		    	$this->db->select('b.GD_ID');
		    	$this->db->select('SP_NAMA');
		    	$this->db->select('KB_NAMA');
		    	$this->db->select('JB_NAMA');
		    	$this->db->select('ST_NAMA');
		    	$this->db->select('GD_NAMA');
		    	$this->db->select('GD_ALAMAT');
		    	$this->db->select('SP_ALAMAT');
		    	$this->db->select('GD_TELP');
		    	$this->db->select('SP_TELP');
	        }
            $this->db->from('bahan as b');
            $this->db->join('supplier as s', 's.SP_ID = b.SP_ID');
            $this->db->join('kat_bahan as kb', 'kb.KB_ID = b.KB_ID');
            $this->db->join('satuan as st', 'st.ST_ID = b.ST_ID');
            $this->db->join('gudang as g', 'g.GD_ID = b.GD_ID');
            $this->db->join('jenis_barang as jb', 'g.JB_ID = jb.JB_ID');
		}

		function get($where = "", $order = "BHN_ID asc", $limit=null, $offset=null, $selectcolumn = true)
		{
  			$this->select($selectcolumn);
  			if($limit != null) $this->db->limit($limit, $offset);
  			if($where != "") $this->db->where($where);
  			$this->db->order_by($order);
  			$query = $this->db->get();
  			return $query->result();
        }
		
        function get_sup($where = "")
        {
        	$this->db->where($where);
			$query = $this->db->get('supplier');
			return $query->result();
		}

		function get_kry($where = "")
		{
			$this->db->where($where);
			$query = $this->db->get('karyawan');
			return $query->result();
		}

		function get_kbhn($where = "")
		{
			$this->db->where($where);
			$query = $this->db->get('kat_bahan');
			return $query->result();
		}

		function get_sat($where = "")
		{
			$this->db->where($where);
			$query = $this->db->get('satuan');
			return $query->result();
		}

		function get_gdg($where = "")
		{
			$this->db->from('gudang g');
			$this->db->join('jenis_barang j', 'g.JB_ID = j.JB_ID');
			$this->db->where($where);
			$query = $this->db->get();
			return $query->result();
		}

		function get_by_id($BHN_ID)
		{
			$this->db->where("id", $BHN_ID);
			$query = $this->db->get('bahan');
			return $query->result();
		}

		function save($data)
		{
			$this->db->insert('bahan', $data);
		}

		function update($BHN_ID, $data)
		{
			$this->db->where("BHN_ID", $BHN_ID);
			$this->db->update('bahan', $data);
		}

		function delete($BHN_ID)
		{
			$data = array();
			$data['BHN_STATUS'] = STATUS_DELETE;
			return $this->db->update('bahan', $data, "BHN_ID = $BHN_ID");
		}
	}
?>
