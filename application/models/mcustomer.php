<?php
	class Mcustomer extends CI_Model {

		function __construct() {
			// Call the Model constructor
			parent:: __construct();
			$this->db = $this->load->database('ballyhoo', TRUE);
			//set waktu yang digunakan ke zona jakarta
			//$this->db->query("SET time_zone='Asia/Jakarta'");
		}
		public function select($selectcolumn=true){
	     	if($selectcolumn){
		    	$this->db->select('CTM_ID');
		    	$this->db->select('CTM_NAMA');
		    	$this->db->select('CTM_ALAMAT');
		    	$this->db->select('CTM_TELP');
		    	$this->db->select('CTM_STATUS');
	        }
            $this->db->from('customer');
		}

		function get($where = "", $order = "CTM_ID asc", $limit=null, $offset=null, $selectcolumn = true){
  			$this->select($selectcolumn);
  			if($limit != null) $this->db->limit($limit, $offset);
  			if($where != "") $this->db->where($where);
  			$this->db->order_by($order);
  			$query = $this->db->get();
  			return $query->result();
        }
		
		function get_by_id($CTM_ID)
		{
			$this->db->where("id", $CTM_ID);
			$query = $this->db->get('customer');
			return $query->result();
		}

		function save($data){
			$this->db->insert('customer', $data);
		}

		function update($CTM_ID, $data)
		{
			$this->db->where("CTM_ID", $CTM_ID);
			$this->db->update('customer', $data);
		}

		function delete($CTM_ID){
			$data = array();
			$data['CTM_STATUS'] = STATUS_DELETE;
			return $this->db->update('customer', $data, "CTM_ID = $CTM_ID");
		}

		function count_all($where = "")
		{
			if($where != null)$this->db->where($where);
			return $this->db->count_all_results('customer');
		}
	}
?>
