<?php
	class Mgudang extends CI_Model {

		function __construct() {
			// Call the Model constructor
			parent:: __construct();
			$this->db = $this->load->database('ballyhoo', TRUE);
			//set waktu yang digunakan ke zona jakarta
			//$this->db->query("SET time_zone='Asia/Jakarta'");
		}
		public function select($selectcolumn=true){
	     	if($selectcolumn){
		    	$this->db->select('GD_ID');
		    	$this->db->select('w.JB_ID');
		    	$this->db->select('it.JB_NAMA');
		    	$this->db->select('GD_NAMA');
		    	$this->db->select('GD_ALAMAT');
		    	$this->db->select('GD_TELP');
		    	$this->db->select('GD_STATUS');
	        }
            $this->db->from('gudang as w');
            $this->db->join('jenis_barang as it', 'w.JB_ID = it.JB_ID');
		}

		public function get_jb($where = ''){
			$this->db->where($where);
			$query = $this->db->get('jenis_barang');
			return $query->result();
		}

		function get($where = "", $order = "GD_ID asc", $limit=null, $offset=null, $selectcolumn = true){
  			$this->select($selectcolumn);
  			if($limit != null) $this->db->limit($limit, $offset);
  			if($where != "") $this->db->where($where);
  			$this->db->order_by($order);
  			$query = $this->db->get();
  			return $query->result();
        }
		
		function get_by_id($GD_ID)
		{
			$this->db->where("id", $GD_ID);
			$query = $this->db->get('gudang');
			return $query->result();
		}

		function save($data){
			$this->db->insert('gudang', $data);
		}

		function update($GD_ID, $data)
		{
			$this->db->where("GD_ID", $GD_ID);
			$this->db->update('gudang', $data);
		}

		function delete($GD_ID){
			$data = array();
			$data['GD_STATUS'] = STATUS_DELETE;
			return $this->db->update('gudang', $data, "GD_ID = $GD_ID");
		}

	}
?>
