<?php
	class Msatuan extends CI_Model {

		function __construct() {
			// Call the Model constructor
			parent:: __construct();
			$this->db = $this->load->database('ballyhoo', TRUE);
			//set waktu yang digunakan ke zona jakarta
			//$this->db->query("SET time_zone='Asia/Jakarta'");
		}
		
		public function select($selectcolumn=true)
		{
	     	if($selectcolumn){
		    	$this->db->select('ST_ID');
		    	$this->db->select('ST_KODE');
		    	$this->db->select('ST_NAMA');
		    	$this->db->select('ST_STATUS');
	        }
            $this->db->from('satuan');
		}

		function get($where = "", $order = "ST_ID asc", $limit=null, $offset=null, $selectcolumn = true)
		{
  			$this->select($selectcolumn);
  			if($limit != null) $this->db->limit($limit, $offset);
  			if($where != "") $this->db->where($where);
  			$this->db->order_by($order);
  			$query = $this->db->get();
  			return $query->result();
        }
		
		function get_by_id($ST_ID)
		{
			$this->db->where("id", $ST_ID);
			$query = $this->db->get('satuan');
			return $query->result();
		}

		function save($data)
		{
			$this->db->insert('satuan', $data);
		}

		function update($ST_ID, $data)
		{
			$this->db->where("ST_ID", $ST_ID);
			$this->db->update('satuan', $data);
		}

		function delete($ST_ID)
		{
			$data = array();
			$data['ST_STATUS'] = STATUS_DELETE;
			return $this->db->update('satuan', $data, "ST_ID = $ST_ID");
		}
	}
?>
