<?php
	class Mkat_produk extends CI_Model {

		function __construct() {
			// Call the Model constructor
			parent:: __construct();
			$this->db = $this->load->database('ballyhoo', TRUE);
			//set waktu yang digunakan ke zona jakarta
			//$this->db->query("SET time_zone='Asia/Jakarta'");
		}
		
		public function select($selectcolumn=true)
		{
	     	if($selectcolumn){
	     		$this->db->select('KPR_ID');
	     		$this->db->select('KPR_NAMA');
		    	$this->db->select('KPR_STATUS');
	        }
	        $this->db->from('kat_produk');
		}

		function get($where = "", $order = "KPR_ID asc", $limit=null, $offset=null, $selectcolumn = true)
		{
  			$this->select($selectcolumn);
  			if($limit != null) $this->db->limit($limit, $offset);
  			if($where != "") $this->db->where($where);
  			$this->db->order_by($order);
  			$query = $this->db->get();
  			return $query->result();
        }
		
		function get_by_id($KPR_ID)
		{
			$this->db->where("id", $KPR_ID);
			$query = $this->db->get('kat_produk');
			return $query->result();
		}

		function save($data)
		{
			$this->db->insert('kat_produk', $data);
		}

		function update($KPR_ID, $data)
		{
			$this->db->where("KPR_ID", $KPR_ID);
			$this->db->update('kat_produk', $data);
		}

		function delete($KPR_ID)
		{
			$data = array();
			$data['KPR_STATUS'] = STATUS_DELETE;
			return $this->db->update('kat_produk', $data, "KPR_ID = $KPR_ID");
		}
	}
?>
