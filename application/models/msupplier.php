<?php
	class Msupplier extends CI_Model {

		function __construct() {
			// Call the Model constructor
			parent:: __construct();
			$this->db = $this->load->database('ballyhoo', TRUE);
			//set waktu yang digunakan ke zona jakarta
			//$this->db->query("SET time_zone='Asia/Jakarta'");
		}
		public function select($selectcolumn=true){
	     	if($selectcolumn){
		    	$this->db->select('SP_ID');
		    	$this->db->select('SP_NAMA');
		    	$this->db->select('SP_ALAMAT');
		    	$this->db->select('SP_TELP');
		    	$this->db->select('SP_STATUS');
	        }
            $this->db->from('supplier');
		}

		function get($where = "", $order = "SP_ID asc", $limit=null, $offset=null, $selectcolumn = true){
  			$this->select($selectcolumn);
  			if($limit != null) $this->db->limit($limit, $offset);
  			if($where != "") $this->db->where($where);
  			$this->db->order_by($order);
  			$query = $this->db->get();
  			return $query->result();
        }
		
		function get_by_id($SP_ID)
		{
			$this->db->where("id", $SP_ID);
			$query = $this->db->get('supplier');
			return $query->result();
		}

		function save($data){
			$this->db->insert('supplier', $data);
		}

		function update($SP_ID, $data)
		{
			$this->db->where("SP_ID", $SP_ID);
			$this->db->update('supplier', $data);
		}

		function delete($SP_ID){
			$data = array();
			$data['SP_STATUS'] = STATUS_DELETE;
			return $this->db->update('supplier', $data, "SP_ID = $SP_ID");
		}

		function count_all($where = "")
		{
			if($where != null)$this->db->where($where);
			return $this->db->count_all_results('supplier');
		}
	}
?>
