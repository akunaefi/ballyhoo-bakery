<?php
	class Mkat_resep extends CI_Model {

		function __construct() {
			// Call the Model constructor
			parent:: __construct();
			$this->db = $this->load->database('ballyhoo', TRUE);
			//set waktu yang digunakan ke zona jakarta
			//$this->db->query("SET time_zone='Asia/Jakarta'");
		}
		
		public function select($selectcolumn=true)
		{
	     	if($selectcolumn){
		    	$this->db->select('KRS_ID');
		    	$this->db->select('KRS_NAMA');
		    	$this->db->select('KRS_STATUS');
	        }
            $this->db->from('kat_resep');
		}

		function get($where = "", $order = "KRS_ID asc", $limit=null, $offset=null, $selectcolumn = true)
		{
  			$this->select($selectcolumn);
  			if($limit != null) $this->db->limit($limit, $offset);
  			if($where != "") $this->db->where($where);
  			$this->db->order_by($order);
  			$query = $this->db->get();
  			return $query->result();
        }
		
		function get_by_id($KRS_ID)
		{
			$this->db->where("id", $KRS_ID);
			$query = $this->db->get('kat_resep');
			return $query->result();
		}

		function save($data)
		{
			$this->db->insert('kat_resep', $data);
		}

		function update($KRS_ID, $data)
		{
			$this->db->where("KRS_ID", $KRS_ID);
			$this->db->update('kat_resep', $data);
		}

		function delete($KRS_ID)
		{
			$data = array();
			$data['KRS_STATUS'] = STATUS_DELETE;
			return $this->db->update('kat_resep', $data, "KRS_ID = $KRS_ID");
		}
	}
?>
