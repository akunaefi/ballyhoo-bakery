<?php
	class Minventory extends CI_Model {

		function __construct() {
			// Call the Model constructor
			parent:: __construct();
			$this->db = $this->load->database('ballyhoo', TRUE);
			//set waktu yang digunakan ke zona jakarta
			//$this->db->query("SET time_zone='Asia/Jakarta'");
		}
	
		public function select($selectcolumn=true)
		{
	     	if($selectcolumn){
	     		$this->db->select('BM_ID');
	     		$this->db->select('BM_TGL');
	     		$this->db->select('BM_QTY');
	     		$this->db->select('BM_HARGA');
	     		$this->db->select('BM_STATUS');
		    	$this->db->select('b.BHN_ID');
		    	$this->db->select('BHN_NAMA');
		    	$this->db->select('BHN_HARGA');
		    	$this->db->select('BHN_STOK');
		    	$this->db->select('BHN_MINSTOK');
		    	$this->db->select('BHN_MINORDER');
		    	$this->db->select('BHN_EXP');
		    	$this->db->select('b.SP_ID');
		    	$this->db->select('b.KB_ID');
		    	$this->db->select('b.ST_ID');
		    	$this->db->select('b.GD_ID');
		    	$this->db->select('SP_NAMA');
		    	$this->db->select('KB_NAMA');
		    	$this->db->select('JB_NAMA');
		    	$this->db->select('ST_NAMA');
		    	$this->db->select('GD_NAMA');
		    	$this->db->select('GD_ALAMAT');
		    	$this->db->select('SP_ALAMAT');
		    	$this->db->select('GD_TELP');
		    	$this->db->select('SP_TELP');
	        }
            $this->db->from('bahan_masuk as bm');
            $this->db->join('bahan as b', 'b.BHN_ID = bm.BHN_ID');
            $this->db->join('supplier as s', 's.SP_ID = b.SP_ID');
            $this->db->join('kat_bahan as kb', 'kb.KB_ID = b.KB_ID');
            $this->db->join('satuan as st', 'st.ST_ID = b.ST_ID');
            $this->db->join('gudang as g', 'g.GD_ID = b.GD_ID');
            $this->db->join('jenis_barang as jb', 'g.JB_ID = jb.JB_ID');
		}

		function get($where = "", $order = "BM_ID asc", $limit=null, $offset=null, $selectcolumn = true)
		{
  			$this->select($selectcolumn);
  			if($limit != null) $this->db->limit($limit, $offset);
  			if($where != "") $this->db->where($where);
  			$this->db->order_by($order);
  			$query = $this->db->get();
  			return $query->result();
        }

		function get_bhn($where = "")
		{	
			$this->db->from('bahan b');
			$this->db->join('supplier s', 'b.SP_ID = s.SP_ID');
			$this->db->where($where);
			$query = $this->db->get();
			return $query->result();
		}

		function get_gdg($where = "")
		{
			$this->db->from('gudang g');
			$this->db->join('jenis_barang j', 'g.JB_ID = j.JB_ID');
			$this->db->where($where);
			$query = $this->db->get();
			return $query->result();
		}

		function get_by_id($BM_ID)
		{
			$this->db->where("id", $BM_ID);
			$query = $this->db->get('bahan_masuk');
			return $query->result();
		}

		function save($data)
		{
			$this->db->insert('bahan_masuk', $data);
		}

		function update($BM_ID, $data)
		{
			$this->db->where("BM_ID", $BM_ID);
			$this->db->update('bahan_masuk', $data);
		}

		function delete($BM_ID)
		{
			$data = array();
			$data['BHN_STATUS'] = STATUS_DELETE;
			return $this->db->update('bahan_masuk', $data, "BM_ID = $BM_ID");
		}
	}
?>
