<?php
	class Muser extends CI_Model {

		function __construct() {
			// Call the Model constructor
			parent:: __construct();
			$this->db = $this->load->database('ballyhoo', TRUE);
			//set waktu yang digunakan ke zona jakarta
			//$this->db->query("SET time_zone='Asia/Jakarta'");
		}
		
		public function select($selectcolumn=true)
		{
	     	if($selectcolumn){
		    	$this->db->select('u.ID_USERS');
		    	$this->db->select('u.ID_EMP');
		    	$this->db->select('emp.NAME_EMP');
		    	$this->db->select('USERSNAME');
		    	$this->db->select('PASSWORD');
		    	$this->db->select('ROLE');
		    	$this->db->select('STATUS_USERS');
	        }
            $this->db->from('users as u');
            $this->db->join('employee as emp', 'emp.ID_EMP = u.ID_EMP');
		}

		function get($where = "", $order = "ID_USERS asc", $limit=null, $offset=null, $selectcolumn = true)
		{
  			$this->select($selectcolumn);
  			if($limit != null) $this->db->limit($limit, $offset);
  			if($where != "") $this->db->where($where);
  			$this->db->order_by($order);
  			$query = $this->db->get();
  			return $query->result();
        }

        public function get_emp($where = "")
		{
            $this->db->where($where);
            $query = $this->db->get('employee');
  			return $query->result();
		}
		
		function get_by_id($ID_USERS)
		{
			$this->db->where("id", $ID_USERS);
			$query = $this->db->get('users');
			return $query->result();
		}

		function save($data)
		{
			$this->db->insert('users', $data);
		}

		function update($ID_USERS, $data)
		{
			$this->db->where("ID_USERS", $ID_USERS);
			$this->db->update('users', $data);
		}

		function delete($ID_USERS)
		{
			$data = array();
			$data['STATUS_USERS'] = STATUS_DELETE;
			return $this->db->update('users', $data, "ID_USERS = $ID_USERS");
		}
	}
?>
