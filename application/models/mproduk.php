<?php
	class Mproduk extends CI_Model {

		function __construct() {
			// Call the Model constructor
			parent:: __construct();
			$this->db = $this->load->database('ballyhoo', TRUE);
			//set waktu yang digunakan ke zona jakarta
			//$this->db->query("SET time_zone='Asia/Jakarta'");
		}
		
		public function select($selectcolumn=true)
		{
	     	if($selectcolumn){
		    	$this->db->select('PRD_ID');
		    	$this->db->select('p.KPR_ID');
		    	$this->db->select('kp.KPR_NAMA');
		    	$this->db->select('p.GD_ID');
		    	$this->db->select('g.GD_NAMA');
		    	$this->db->select('PRD_NAMA');
		    	$this->db->select('PRD_HARGA');
		    	$this->db->select('PRD_STOK');
		    	$this->db->select('PRD_STATUS');
	        }
            $this->db->from('produk as p');
            $this->db->join('kat_produk as kp', 'p.KPR_ID = kp.KPR_ID');
            $this->db->join('gudang as g', 'p.GD_ID = g.GD_ID');
		}

		function get($where = "", $order = "PRD_ID asc", $limit=null, $offset=null, $selectcolumn = true)
		{
  			$this->select($selectcolumn);
  			if($limit != null) $this->db->limit($limit, $offset);
  			if($where != "") $this->db->where($where);
  			$this->db->order_by($order);
  			$query = $this->db->get();
  			return $query->result();
        }
		
        function get_gdg($where = "")
		{
		    $this->db->select('GD_ID');
		    $this->db->select('w.JB_ID');
		    $this->db->select('it.JB_NAMA');
		    $this->db->select('GD_NAMA');
		    $this->db->select('GD_ALAMAT');
		    $this->db->select('GD_TELP');
		    $this->db->select('GD_STATUS');
            $this->db->from('gudang as w');
            $this->db->join('jenis_barang as it', 'w.JB_ID = it.JB_ID');
			$this->db->where($where);
			$query = $this->db->get();
			return $query->result();
		}

		function get_kp($where = "")
		{
			$this->db->where($where);
			$query = $this->db->get('kat_produk');
			return $query->result();
		}

		function get_jb($where = "")
		{
			$this->db->where($where);
			$query = $this->db->get('jenis_barang');
			return $query->result();
		}

		function get_by_id($PRD_ID)
		{
			$this->db->where("id", $PRD_ID);
			$query = $this->db->get('produk');
			return $query->result();
		}

		function save($data)
		{
			$this->db->insert('produk', $data);
		}

		function update($PRD_ID, $data)
		{
			$this->db->where("PRD_ID", $PRD_ID);
			$this->db->update('produk', $data);
		}

		function delete($PRD_ID)
		{
			$data = array();
			$data['PRD_STATUS'] = STATUS_DELETE;
			return $this->db->update('produk', $data, "PRD_ID = $PRD_ID");
		}
	}
?>
