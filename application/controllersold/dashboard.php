<?php
/**
 * class untuk handle jnsab
 * Ahmad Yusuf
 */
class Dashboard extends MY_Controller {
   //constructor class
    // public function __construct() {
    //   parent::__construct();
    //   //if(!$this->auth->validate(true)) exit(0);
    //   //$this->cek_login();
    //   //$this->load->model('mbuatlaporan');
    //   //$this->breadcrumb->add(lang_value('jnsab_label'), 'jnsab');
    // }

  public function index(){
    $this->load->view('base/header');
    $this->load->view('base/menu_admin');
    $this->load->view('dashboard/dashboard');
    $this->load->view('base/footer');
  }

  public function save()
  {
    $data=$this->input->post();

      $nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
      $config['upload_path'] = './assets/uploads/laporan/'; //path folder
      $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
      $config['max_size'] = '3072'; //maksimum besar file 3M
      $config['max_width']  = '5000'; //lebar maksimum 5000 px
      $config['max_height']  = '5000'; //tinggi maksimu 5000 px
      $config['file_name'] = $nmfile; //nama yang terupload nantinya
      $this->load->library('upload' , $config);

          if ($this->upload->do_upload('tpt_foto'))
          {
              $gbr = $this->upload->data();
              $data['tpt_foto'] = $gbr['file_name'];
              //fungsi resizer
              $config2['image_library'] = 'gd2';
              $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
              $config2['new_image'] = './assets/hasil_resize/laporan/'; // folder tempat menyimpan hasil resize
              $config2['maintain_ratio'] = TRUE;
              $config2['width'] = 200; //lebar setelah resize menjadi 100 px
              $config2['height'] = 200; //lebar setelah resize menjadi 100 px
              $this->load->library('image_lib',$config2);

              //pesan yang muncul jika resize error dimasukkan pada session flashdata
              if ( !$this->image_lib->resize()){
              $this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
            }
              //pesan yang muncul jika berhasil diupload pada session flashdata
              $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Upload gambar berhasil !!</div></div>");

          }else{
              //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
              // $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Gagal upload gambar !!</div></div>");
              // redirect('upload/add'); //jika gagal maka akan ditampilkan form upload
              $error = array('error' => $this->upload->display_errors());

          }

        //image uploader by gusti

        /// resizer

        // end
      $LPR_ID = $this->mbuatlaporan->insert(
        $this->input->post('LPR_PELAPOR'),
        $this->input->post('KTG_ID'),
        $this->input->post('LPR_URGENSI'),
        $this->input->post('LPR_SUBJECT'),
        $this->input->post('LPR_DESKRIPSI'),
        $data['tpt_foto']
        // $this->input->post('LPR_GAMBAR');
      );


      redirect(base_url().'laporan-keluar');
  }

}

