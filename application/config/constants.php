<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */

define('FPDF_FONTPATH', 'assets/fonts/');

//nama database simpeg
define('DB_SIMPEG', 'simpeg_0511');
define('DB_REMUN', 'remun');

//status delete
define('ACTIVE', 0);
define('DEL', 1);

// STATUS
define('STATUS_ACTIVE', 1);
define('STATUS_DELETE', 2);

//jenis surat
define('LAPORAN_DITERIMA', 1);
define('LAPORAN_MENUNGGU', 2);
define('LAPORAN_DITOLAK', 3);

//jenis surat
define('SURAT_MASUK', 1);
define('SURAT_KELUAR', 2);
define('SURAT_KEPUTUSAN', 3);
define('SURAT_LAINNYA', 4);

// srt msk kategori
define('LUAR', 1);
define('INTERNAL', 2);

// srt msk status
define('DRAFT', 0);
define('PROSES', 1);
define('SELESAI', 2);
define('HAPUS', 3);

//validasi
define('VALIDASI_PENDING', 1);
define('VALIDASI_VALID', 2);
define('VALIDASI_GAGAL', 3);

//satuan masa
define('SATUAN_SEMESTER', 'Semester');
define('SATUAN_TAHUN', 'Tahun');


//bidang bkd
define('PENDIDIKAN', 1); //Pendidikan dan Pengajaran
define('PENELITIAN', 2); //Penelitian
define('PENGABDIAN', 3); //Pengabdian Masyarakat
define('PENUNJANG', 4); //Penunjang
define('PROFESOR', 5); //Kewajiban Profesor

//satuan masa kegiatan
define('KEGIATAN_HARI', 1);//Hari
define('KEGIATAN_BULAN', 2);//Bulan / OB
define('KEGIATAN_SEMESTER', 3);//Semester
define('KEGIATAN_TAHUN', 4);//Tahun
define('KEGIATAN_OK', 5);//Kegiatan / OK


//jenis dosen
define('DS', 1); //DS
define('DT', 2); //DT
define('PS', 3); //PS
define('PT', 4); //PT


//semester
define('GASAL', 1); //gasal
define('GENAP', 2); //genap

//Periode SKP
define('SATU', 1);
define('DUA', 2);
