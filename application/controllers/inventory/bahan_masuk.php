<?php

class Bahan_masuk extends MY_Controller {
   //constructor class
    public function __construct() {
      parent::__construct();
    //   //if(!$this->auth->validate(true)) exit(0);
    //   //$this->cek_login();
      $this->load->model('mbahan_masuk');
    //   //$this->breadcrumb->add(lang_value('jnsab_label'), 'jnsab');
      $this->load->helper('base_helper');
      //set waktu yang digunakan ke zona jakarta
      date_default_timezone_set('Asia/Jakarta');
    }

    public function index()
    {
      $data['bahan_masuk'] = $this->mbahan_masuk->get('BM_STATUS = '.STATUS_ACTIVE);
      $data['bahan'] = $this->mbahan_masuk->get_bhn('BHN_STATUS = '.STATUS_ACTIVE);
      $data['sat'] = $this->mbahan_masuk->get_sat('ST_STATUS = '.STATUS_ACTIVE);
      $this->load->view('base/header');
      $this->load->view('base/menu_admin');
      $this->load->view('inventory/bahan_masuk', $data);
      $this->load->view('base/footer');
    }

    public function delete($BM_ID){
      if(!$this->input->is_ajax_request()) show_404();

      if($BM_ID)
      {
        $this->mbahan_masuk->delete($BM_ID);
      }
      else
      {
        ajax_response('failed', 'Gagal');
      }

      ajax_response();
    }

    public function get_by_id()
    {
      $output = array();
      $data = $this->mbahan_masuk->get('BM_ID = '.$_POST["BM_ID"]);
      foreach ($data as $row) 
      {
        $output['BM_ID'] = $row->BM_ID;
        $output['BM_TGL'] = $row->BM_TGL;
        $output['BM_QTY'] = $row->BM_QTY;
        $output['BM_STATUS'] = $row->BM_STATUS;
        $output['BHN_ID'] = $row->BHN_ID;
        $output['BHN_NAMA'] = $row->BHN_NAMA;
        $output['BHN_HARGA'] = $row->BHN_HARGA;
        $output['BHN_STOK'] = $row->BHN_STOK;
        $output['BHN_MINSTOK'] = $row->BHN_MINSTOK;
        $output['BHN_MINORDER'] = $row->BHN_MINORDER;
        $output['BHN_EXP'] = $row->BHN_EXP;
        $output['ST_NAMA'] = $row->ST_NAMA;
        $output['SP_ID'] = $row->SP_ID;
        $output['GD_ID'] = $row->GD_ID;
        $output['GD_NAMA'] = $row->GD_NAMA;
        $output['SP_NAMA'] = $row->SP_NAMA;
        $output['GD_ALAMAT'] = $row->GD_ALAMAT;
        $output['SP_ALAMAT'] = $row->SP_ALAMAT;
        $output['GD_TELP'] = $row->GD_TELP;
        $output['SP_TELP'] = $row->SP_TELP;
      }
      echo json_encode($output);
    }

    public function action()
    {
        $save_data = array(
          //'BM_ID' => $this->input->post('ID'),
          'BM_TGL' => now(),
          'BM_QTY' => $this->input->post('BM_QTY'),
          'BHN_ID' => $this->input->post('BHN_ID'),
          'GD_ID' => $this->input->post('GD_ID'),
          'BM_STATUS'      => STATUS_ACTIVE
          );
        $this->mbahan_masuk->save($save_data);
        $this->mbahan_masuk->update_stok($this->input->post("BHN_ID"), $this->input->post('BM_QTY'));
     
    }
}