<?php

class Bahan_keluar extends MY_Controller {
   //constructor class
    public function __construct() {
      parent::__construct();
    //   //if(!$this->auth->validate(true)) exit(0);
    //   //$this->cek_login();
      $this->load->model('mbahan_keluar');
    //   //$this->breadcrumb->add(lang_value('jnsab_label'), 'jnsab');
      $this->load->helper('base_helper');
      //set waktu yang digunakan ke zona jakarta
      date_default_timezone_set('Asia/Jakarta');
    }

    public function index()
    {
      $data['bahan_keluar'] = $this->mbahan_keluar->get('BK_STATUS = '.STATUS_ACTIVE);
      $data['bahan'] = $this->mbahan_keluar->get_bhn('BHN_STATUS = '.STATUS_ACTIVE);
      $data['sat'] = $this->mbahan_keluar->get_sat('ST_STATUS = '.STATUS_ACTIVE);
      $this->load->view('base/header');
      $this->load->view('base/menu_admin');
      $this->load->view('inventory/bahan_keluar', $data);
      $this->load->view('base/footer');
    }

    public function delete($BK_ID){
      if(!$this->input->is_ajax_request()) show_404();

      if($BK_ID)
      {
        $this->mbahan_keluar->delete($BK_ID);
      }
      else
      {
        ajax_response('failed', 'Gagal');
      }

      ajax_response();
    }

    public function get_by_id()
    {
      $output = array();
      $data = $this->mbahan_keluar->get('BK_ID = '.$_POST["BK_ID"]);
      foreach ($data as $row) 
      {
        $output['BK_ID'] = $row->BK_ID;
        $output['BK_TGL'] = $row->BK_TGL;
        $output['BK_QTY'] = $row->BK_QTY;
        $output['BK_STATUS'] = $row->BK_STATUS;
        $output['BHN_ID'] = $row->BHN_ID;
        $output['BHN_NAMA'] = $row->BHN_NAMA;
        $output['BHN_HARGA'] = $row->BHN_HARGA;
        $output['BHN_STOK'] = $row->BHN_STOK;
        $output['BHN_MINSTOK'] = $row->BHN_MINSTOK;
        $output['BHN_MINORDER'] = $row->BHN_MINORDER;
        $output['BHN_EXP'] = $row->BHN_EXP;
        $output['ST_NAMA'] = $row->ST_NAMA;
        $output['SP_ID'] = $row->SP_ID;
        $output['GD_ID'] = $row->GD_ID;
        $output['GD_NAMA'] = $row->GD_NAMA;
        $output['SP_NAMA'] = $row->SP_NAMA;
        $output['GD_ALAMAT'] = $row->GD_ALAMAT;
        $output['SP_ALAMAT'] = $row->SP_ALAMAT;
        $output['GD_TELP'] = $row->GD_TELP;
        $output['SP_TELP'] = $row->SP_TELP;
      }
      echo json_encode($output);
    }

    public function action()
    {
      
        $save_data = array(
          //'BK_ID' => $this->input->post('ID'),
          'BK_TGL' => now(),
          'BK_QTY' => $this->input->post('BK_QTY'),
          'BHN_ID' => $this->input->post('BHN_ID'),
          'GD_ID' => $this->input->post('GD_ID'),
          'BK_STATUS'      => STATUS_ACTIVE
          );
        $this->mbahan_keluar->save($save_data);
        $this->mbahan_keluar->update_stok($this->input->post("BHN_ID"), $this->input->post('BK_QTY'));
      
      
    }
}