<?php

  class Karyawan extends MY_Controller {
   //constructor class
    public function __construct() {
      parent::__construct();
    //   //if(!$this->auth->validate(true)) exit(0);
    //   //$this->cek_login();
      $this->load->model('mkaryawan');
    //   //$this->breadcrumb->add(lang_value('jnsab_label'), 'jnsab');
    }

    public function index()
    {
      $data['karyawan'] = $this->mkaryawan->get('KR_STATUS = '.STATUS_ACTIVE);
      $data['kp'] = $this->mkaryawan->get_pos('KP_STATUS = '.STATUS_ACTIVE);
      $this->load->view('base/header');
      $this->load->view('base/menu_admin');
      $this->load->view('data/karyawan', $data);
      $this->load->view('base/footer');
    }

    public function delete($KR_ID){
      if(!$this->input->is_ajax_request()) show_404();

      if($KR_ID)
      {
        $this->mkaryawan->delete($KR_ID);
      }
      else
      {
        ajax_response('failed', 'Gagal');
      }

      ajax_response();
    }

    public function get_by_id()
    {
      $output = array();
      $data = $this->mkaryawan->get('KR_ID = '.$_POST["KR_ID"]);
      foreach ($data as $row) 
      {
        $output['KR_ID'] = $row->KR_ID;
        $output['KP_ID'] = $row->KP_ID;
        $output['KR_NAMA'] = $row->KR_NAMA;
        $output['KR_ALAMAT'] = $row->KR_ALAMAT;
        $output['KR_TELP'] = $row->KR_TELP;
        $output['KR_USERNAME'] = $row->KR_USERNAME;
        $output['KR_PASSWORD'] = $row->KR_PASSWORD;
      }
      echo json_encode($output);
    }

    public function action()
    {
      if ($this->input->post('ID') == '') {
        $save_data = array(
          'KR_NAMA'      => $this->input->post('KR_NAMA'),
          'KP_ID'    => $this->input->post('KP_ID'),
          'KR_ALAMAT'   => $this->input->post('KR_ALAMAT'),
          'KR_TELP'      => $this->input->post('KR_TELP'),
          'KR_USERNAME'      => 'user',
          'KR_PASSWORD'      => '1234',
          'KR_STATUS'    => STATUS_ACTIVE
          );
        $this->mkaryawan->save($save_data);
      } else {
        $updated_data = array(
          'KR_NAMA'      => $this->input->post('KR_NAMA'),
          'KP_ID'    => $this->input->post('KP_ID'),
          'KR_ALAMAT'   => $this->input->post('KR_ALAMAT'),
          'KR_USERNAME'      => $this->input->post('KR_USERNAME'),
          'KR_PASSWORD'      => $this->input->post('KR_PASSWORD'),
          'KR_TELP'      => $this->input->post('KR_TELP')
        );
        $this->mkaryawan->update($this->input->post("ID"), $updated_data);
      }
      
    }
}