<?php

class Bahan extends MY_Controller {
   //constructor class
    public function __construct() {
      parent::__construct();
    //   //if(!$this->auth->validate(true)) exit(0);
    //   //$this->cek_login();
      $this->load->model('mbahan');
      $this->load->helper('base_helper');
      //set waktu yang digunakan ke zona jakarta
      date_default_timezone_set('Asia/Jakarta');
    //   //$this->breadcrumb->add(lang_value('jnsab_label'), 'jnsab');
    }

    public function index()
    {
      $data['bahan'] = $this->mbahan->get('BHN_STATUS = '.STATUS_ACTIVE);
      $data['sup'] = $this->mbahan->get_sup('SP_STATUS = '.STATUS_ACTIVE);
      $data['kry'] = $this->mbahan->get_kry('KR_STATUS = '.STATUS_ACTIVE);
      $data['kbhn'] = $this->mbahan->get_kbhn('KB_STATUS = '.STATUS_ACTIVE);
      $data['sat'] = $this->mbahan->get_sat('ST_STATUS = '.STATUS_ACTIVE);
      $data['gdg'] = $this->mbahan->get_gdg('GD_STATUS = '.STATUS_ACTIVE);
      $this->load->view('base/header');
      $this->load->view('base/menu_admin');
      $this->load->view('data/bahan', $data);
      $this->load->view('base/footer');
    }

    public function delete($BHN_ID){
      if(!$this->input->is_ajax_request()) show_404();

      if($BHN_ID)
      {
        $this->mbahan->delete($BHN_ID);
      }
      else
      {
        ajax_response('failed', 'Gagal');
      }

      ajax_response();
    }

    public function get_by_id()
    {
      $output = array();
      $data = $this->mbahan->get('BHN_ID = '.$_POST["BHN_ID"]);
      foreach ($data as $row) 
      {
        $output['BHN_ID'] = $row->BHN_ID;
        $output['BHN_NAMA'] = $row->BHN_NAMA;
        $output['BHN_HARGA'] = $row->BHN_HARGA;
        $output['BHN_STOK'] = $row->BHN_STOK;
        $output['BHN_MINSTOK'] = $row->BHN_MINSTOK;
        $output['BHN_MINORDER'] = $row->BHN_MINORDER;
        $output['BHN_EXP'] = $row->BHN_EXP;
        $output['BHN_STATUS'] = $row->BHN_STATUS;
        $output['SP_ID'] = $row->SP_ID;
        $output['KB_ID'] = $row->KB_ID;
        $output['ST_ID'] = $row->ST_ID;
        $output['GD_ID'] = $row->GD_ID;
        $output['GD_NAMA'] = $row->GD_NAMA;
        $output['SP_NAMA'] = $row->SP_NAMA;
        $output['ST_NAMA'] = $row->ST_NAMA;
        $output['KB_NAMA'] = $row->KB_NAMA;
        $output['JB_NAMA'] = $row->JB_NAMA;
        $output['GD_ALAMAT'] = $row->GD_ALAMAT;
        $output['SP_ALAMAT'] = $row->SP_ALAMAT;
        $output['GD_TELP'] = $row->GD_TELP;
        $output['SP_TELP'] = $row->SP_TELP;
        if ($row->BHN_STOK > $row->BHN_MINSTOK) {
          $output['stok'] = 'has-success';
        } else if ($row->BHN_STOK == $row->BHN_MINSTOK) {
          $output['stok'] = 'has-warning';
        } else {
          $output['stok'] = 'has-error';
        }
      }
      echo json_encode($output);
    }

    public function action()
    {
      if ($this->input->post('ID') == '') {
        $save_data = array(
          'BHN_ID' => $this->input->post('ID'),
          'BHN_NAMA' => $this->input->post('BHN_NAMA'),
          'BHN_HARGA' => $this->input->post('BHN_HARGA'),
          'BHN_STOK' => $this->input->post('BHN_STOK'),
          'BHN_MINSTOK' => $this->input->post('BHN_MINSTOK'),
          'BHN_MINORDER' => $this->input->post('BHN_MINORDER'),
          'BHN_EXP' => $this->input->post('BHN_EXP'),
          'BHN_UPDATE' => now(),
          'SP_ID' => $this->input->post('SP_ID'),
          'KB_ID' => $this->input->post('KB_ID'),
          'ST_ID' => $this->input->post('ST_ID'),
          'GD_ID' => $this->input->post('GD_ID'),
          'BHN_STATUS'      => STATUS_ACTIVE
          );
        $this->mbahan->save($save_data);
      } else {
        $updated_data = array(
          'BHN_ID' => $this->input->post('ID'),
          'BHN_NAMA' => $this->input->post('BHN_NAMA'),
          'BHN_HARGA' => $this->input->post('BHN_HARGA'),
          'BHN_STOK' => $this->input->post('BHN_STOK'),
          'BHN_MINSTOK' => $this->input->post('BHN_MINSTOK'),
          'BHN_MINORDER' => $this->input->post('BHN_MINORDER'),
          'BHN_EXP' => $this->input->post('BHN_EXP'),
          'SP_ID' => $this->input->post('SP_ID'),
          'KB_ID' => $this->input->post('KB_ID'),
          'ST_ID' => $this->input->post('ST_ID'),
          'GD_ID' => $this->input->post('GD_ID'),
        );
        $this->mbahan->update($this->input->post("ID"), $updated_data);
      }
      
    }
}