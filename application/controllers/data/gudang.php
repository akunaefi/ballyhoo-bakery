<?php

class Gudang extends MY_Controller {
   //constructor class
    public function __construct() {
      parent::__construct();
    //   //if(!$this->auth->validate(true)) exit(0);
    //   //$this->cek_login();
      $this->load->model('mgudang');
    //   //$this->breadcrumb->add(lang_value('jnsab_label'), 'jnsab');
    }

    public function index(){
      $data['warehouse'] = $this->mgudang->get('GD_STATUS = '.STATUS_ACTIVE);
      $data['item_type'] = $this->mgudang->get_jb('JB_STATUS = '.STATUS_ACTIVE);
      $this->load->view('base/header');
      $this->load->view('base/menu_admin');
      $this->load->view('data/gudang', $data);
      $this->load->view('base/footer');
    }

    public function delete($GD_ID)
    {
      if(!$this->input->is_ajax_request()) show_404();

      if($GD_ID)
      {
        $this->mgudang->delete($GD_ID);
      }
      else
      {
        ajax_response('failed', 'Gagal');
      }

      ajax_response();
    }

    public function get_by_id()
    {
      $output = array();
      $data = $this->mgudang->get('GD_ID = '.$_POST["GD_ID"]);
      foreach ($data as $row) 
      {
        $output['GD_ID'] = $row->GD_ID;
        $output['GD_NAMA'] = $row->GD_NAMA;
        $output['GD_ALAMAT'] = $row->GD_ALAMAT;
        $output['JB_ID'] = $row->JB_ID;
        $output['GD_TELP'] = $row->GD_TELP;
      }
      echo json_encode($output);
    }

    public function action()
    {
      if ($this->input->post('ID') == '') {
        $save_data = array(
          'JB_ID'      => $this->input->post('JB_ID'),
          'GD_NAMA'    => $this->input->post('GD_NAMA'),
          'GD_ALAMAT' => $this->input->post('GD_ALAMAT'),
          'GD_TELP'    => $this->input->post('GD_TELP'),
          'GD_STATUS'  => STATUS_ACTIVE
          );
        $this->mgudang->save($save_data);
      } else {
        $updated_data = array(
          'JB_ID'      => $this->input->post('JB_ID'),
          'GD_NAMA'    => $this->input->post('GD_NAMA'),
          'GD_ALAMAT' => $this->input->post('GD_ALAMAT'),
          'GD_TELP'    => $this->input->post('GD_TELP')
        );
        $this->mgudang->update($this->input->post("ID"), $updated_data);
      }
      
    }
}