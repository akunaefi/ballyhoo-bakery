
<?php

class Supplier extends MY_Controller {
   //constructor class
    public function __construct() {
      parent::__construct();
    //   //if(!$this->auth->validate(true)) exit(0);
    //   //$this->cek_login();
      $this->load->model('msupplier');
    //   //$this->breadcrumb->add(lang_value('jnsab_label'), 'jnsab');
    }

    public function index(){
      $data['supp'] = $this->msupplier->get('SP_STATUS = '.STATUS_ACTIVE);
      $this->load->view('base/header');
      $this->load->view('base/menu_admin');
      $this->load->view('data/supplier', $data);
      $this->load->view('base/footer');
    }

    public function delete($SP_ID)
    {
      if(!$this->input->is_ajax_request()) show_404();

      if($SP_ID)
      {
        $this->msupplier->delete($SP_ID);
      }
      else
      {
        ajax_response('failed', 'Gagal');
      }

      ajax_response();
    }

    public function get_by_id()
    {
      $output = array();
      $data = $this->msupplier->get('SP_ID = '.$_POST["SP_ID"]);
      foreach ($data as $row) 
      {
        $output['SP_ID'] = $row->SP_ID;
        $output['SP_NAMA'] = $row->SP_NAMA;
        $output['SP_ALAMAT'] = $row->SP_ALAMAT;
        $output['SP_TELP'] = $row->SP_TELP;
      }
      echo json_encode($output);
    }

    public function action()
    {
      if ($this->input->post('ID') == '') {
        $save_data = array(
          'SP_NAMA'     => $this->input->post('SP_NAMA'),
          'SP_ALAMAT'   => $this->input->post('SP_ALAMAT'),
          'SP_TELP'     => $this->input->post('SP_TELP'),
          'SP_STATUS'   => STATUS_ACTIVE
          );
        $this->msupplier->save($save_data);
      } else {
        $updated_data = array(
          'SP_NAMA'     => $this->input->post('SP_NAMA'),
          'SP_ALAMAT'   => $this->input->post('SP_ALAMAT'),
          'SP_TELP'     => $this->input->post('SP_TELP')
        );
        $this->msupplier->update($this->input->post("ID"), $updated_data);
      }
      
    }
}