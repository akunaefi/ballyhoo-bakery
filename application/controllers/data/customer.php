<?php

  class Customer extends MY_Controller {
   //constructor class
    public function __construct() {
      parent::__construct();
      //$this->cek_login();
      $this->load->model('mcustomer');
    }

    public function index(){
      $data['customer'] = $this->mcustomer->get('CTM_STATUS = '.STATUS_ACTIVE);
      $this->load->view('base/header');
      $this->load->view('base/menu_admin');
      $this->load->view('data/customer', $data);
      $this->load->view('base/footer');
    }

    public function delete($CTM_ID)
    {
      if(!$this->input->is_ajax_request()) show_404();

      if($CTM_ID)
      {
        $this->mcustomer->delete($CTM_ID);
      }
      else
      {
        ajax_response('failed', 'Gagal');
      }

      ajax_response();
    }

    public function get_by_id()
    {
      $output = array();
      $data = $this->mcustomer->get('CTM_ID = '.$_POST["CTM_ID"]);
      foreach ($data as $row) 
      {
        $output['CTM_ID'] = $row->CTM_ID;
        $output['CTM_NAMA'] = $row->CTM_NAMA;
        $output['CTM_ALAMAT'] = $row->CTM_ALAMAT;
        $output['CTM_TELP'] = $row->CTM_TELP;
      }
      echo json_encode($output);
    }

    public function action()
    {
      if ($this->input->post('ID') == '') {
        $save_data = array(
          'CTM_NAMA'    => $this->input->post('CTM_NAMA'),
          'CTM_ALAMAT' => $this->input->post('CTM_ALAMAT'),
          'CTM_TELP'    => $this->input->post('CTM_TELP'),
          'CTM_STATUS'  => STATUS_ACTIVE
          );
        $this->mcustomer->save($save_data);
      } else {
        $updated_data = array(
          'CTM_NAMA'    => $this->input->post('CTM_NAMA'),
          'CTM_ALAMAT' => $this->input->post('CTM_ALAMAT'),
          'CTM_TELP'    => $this->input->post('CTM_TELP')
        );
        $this->mcustomer->update($this->input->post("ID"), $updated_data);
      }
      
    }
  }