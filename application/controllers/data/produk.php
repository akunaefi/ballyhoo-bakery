<?php

class Produk extends MY_Controller {
   //constructor class
    public function __construct() {
      parent::__construct();
    //   //if(!$this->auth->validate(true)) exit(0);
    //   //$this->cek_login();
      $this->load->model('mproduk');
    //   //$this->breadcrumb->add(lang_value('jnsab_label'), 'jnsab');
    }

    public function index()
    {
      $data['produk'] = $this->mproduk->get('PRD_STATUS = '.STATUS_ACTIVE);
      $data['gudang'] = $this->mproduk->get_gdg('GD_STATUS = '.STATUS_ACTIVE);
      $data['kp'] = $this->mproduk->get_kp('KPR_STATUS = '.STATUS_ACTIVE);
      $data['jb'] = $this->mproduk->get_jb('JB_STATUS = '.STATUS_ACTIVE);
      $this->load->view('base/header');
      $this->load->view('base/menu_admin');
      $this->load->view('data/produk', $data);
      $this->load->view('base/footer');
    }

    public function delete($PRD_ID){
      if(!$this->input->is_ajax_request()) show_404();

      if($PRD_ID)
      {
        $this->mproduk->delete($PRD_ID);
      }
      else
      {
        ajax_response('failed', 'Gagal');
      }

      ajax_response();
    }

    public function get_by_id()
    {
      $output = array();
      $data = $this->mproduk->get('PRD_ID = '.$_POST["PRD_ID"]);
      foreach ($data as $row) 
      {
        $output['PRD_ID'] = $row->PRD_ID;
        $output['KPR_ID'] = $row->KPR_ID;
        $output['GD_ID'] = $row->GD_ID;
        $output['PRD_NAMA'] = $row->PRD_NAMA;
        $output['PRD_HARGA'] = $row->PRD_HARGA;
        $output['PRD_STOK'] = $row->PRD_STOK;
      }
      echo json_encode($output);
    }

    public function action()
    {
      if ($this->input->post('ID') == '') {
        $save_data = array(
          'PRD_ID'      => $this->input->post('PRD_ID'),
          'GD_ID'      => $this->input->post('GD_ID'),
          'KPR_ID'          => $this->input->post('KPR_ID'),
          'PRD_NAMA'        => $this->input->post('PRD_NAMA'),
          'PRD_HARGA'      => $this->input->post('PRD_HARGA'),
          'PRD_STOK'       => $this->input->post('PRD_STOK'),
          'PRD_STATUS'      => STATUS_ACTIVE
          );
        $this->mproduk->save($save_data);
      } else {
        $updated_data = array(
          'PRD_ID'      => $this->input->post('PRD_ID'),
          'GD_ID'      => $this->input->post('GD_ID'),
          'KPR_ID'          => $this->input->post('KPR_ID'),
          'PRD_NAMA'        => $this->input->post('PRD_NAMA'),
          'PRD_HARGA'      => $this->input->post('PRD_HARGA'),
          'PRD_STOK'       => $this->input->post('PRD_STOK')
        );
        $this->mproduk->update($this->input->post("ID"), $updated_data);
      }
      
    }
}