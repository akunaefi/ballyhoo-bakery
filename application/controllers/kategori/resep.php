<?php

class Resep extends MY_Controller {
   //constructor class
    public function __construct() {
      parent::__construct();
    //   //if(!$this->auth->validate(true)) exit(0);
    //   //$this->cek_login();
      $this->load->model('mkat_resep');
    //   //$this->breadcrumb->add(lang_value('jnsab_label'), 'jnsab');
    }

    public function index()
    {
      $data['rec_cat'] = $this->mkat_resep->get('KRS_STATUS = '.STATUS_ACTIVE);
      $this->load->view('base/header');
      $this->load->view('base/menu_admin');
      $this->load->view('kategori/resep', $data);
      $this->load->view('base/footer');
    }

    public function delete($KRS_ID)
    {
      if(!$this->input->is_ajax_request()) show_404();

      if($KRS_ID)
      {
        $this->mkat_resep->delete($KRS_ID);
      }
      else
      {
        ajax_response('failed', 'Gagal');
      }

      ajax_response();
    }

    public function get_by_id()
    {
      $output = array();
      $data = $this->mkat_resep->get('KRS_ID = '.$_POST["KRS_ID"]);
      foreach ($data as $row) 
      {
        $output['KRS_ID'] = $row->KRS_ID;
        $output['KRS_NAMA'] = $row->KRS_NAMA;
      }
      echo json_encode($output);
    }

    public function action()
    {
      if ($this->input->post('ID') == '') {
        $save_data = array(
          'KRS_NAMA'     => $this->input->post('KRS_NAMA'),
          'KRS_STATUS'   => STATUS_ACTIVE
          );
        $this->mkat_resep->save($save_data);
      } else {
        $updated_data = array(
          'KRS_NAMA'     => $this->input->post('KRS_NAMA')
        );
        $this->mkat_resep->update($this->input->post("ID"), $updated_data);
      }
      
    }
}