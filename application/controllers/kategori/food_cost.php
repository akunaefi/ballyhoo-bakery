<?php

class Food_cost extends MY_Controller {
   //constructor class
    public function __construct() {
      parent::__construct();
    //   //if(!$this->auth->validate(true)) exit(0);
    //   //$this->cek_login();
      $this->load->model('mfood_cost');
    //   //$this->breadcrumb->add(lang_value('jnsab_label'), 'jnsab');
    }

    public function index(){
      $data['food_cost'] = $this->mfood_cost->get('FC_STATUS = '.STATUS_ACTIVE);
      $this->load->view('base/header');
      $this->load->view('base/menu_admin');
      $this->load->view('kategori/food_cost', $data);
      $this->load->view('base/footer');
    }

    public function delete($FC_ID){
      if(!$this->input->is_ajax_request()) show_404();

      if($FC_ID)
      {
        $this->mfood_cost->delete($FC_ID);
      }
      else
      {
        ajax_response('failed', 'Gagal');
      }

      ajax_response();
    }

    public function get_by_id()
    {
      $output = array();
      $data = $this->mfood_cost->get('FC_ID = '.$_POST["FC_ID"]);
      foreach ($data as $row) 
      {
        $output['FC_ID'] = $row->FC_ID;
        $output['FC_NAMA'] = $row->FC_NAMA;
        $output['FC_VALUE'] = $row->FC_VALUE;
      }
      echo json_encode($output);
    }

    public function action()
    {
      if ($this->input->post('ID') == '') {
        $save_data = array(
          'FC_NAMA'     => $this->input->post('FC_NAMA'),
          'FC_VALUE'     => $this->input->post('FC_VALUE'),
          'FC_STATUS'   => STATUS_ACTIVE
          );
        $this->mfood_cost->save($save_data);
      } else {
        $updated_data = array(
          'FC_NAMA'     => $this->input->post('FC_NAMA'),
          'FC_VALUE'     => $this->input->post('FC_VALUE')
        );
        $this->mfood_cost->update($this->input->post("ID"), $updated_data);
      }
      
    }


}