<?php

  class Jenis_barang extends MY_Controller {
   //constructor class
    public function __construct() 
    {
      parent::__construct();
    //   //if(!$this->auth->validate(true)) exit(0);
    //   //$this->cek_login();
      $this->load->model('mjenis_barang');
    //   //$this->breadcrumb->add(lang_value('jnsab_label'), 'jnsab');
    }

    public function index()
    {
      $data['jenis_barang'] = $this->mjenis_barang->get('JB_STATUS = '.STATUS_ACTIVE);
      $this->load->view('base/header');
      $this->load->view('base/menu_admin');
      $this->load->view('kategori/jenis_barang', $data);
      $this->load->view('base/footer');
    }

    public function delete($JB_ID)
    {
      if(!$this->input->is_ajax_request()) show_404();

      if($JB_ID)
      {
        $this->mjenis_barang->delete($JB_ID);
      }
      else
      {
        ajax_response('failed', 'Gagal');
      }

      ajax_response();
    }

    public function get_by_id()
    {
      $output = array();
      $data = $this->mjenis_barang->get('JB_ID = '.$_POST["JB_ID"]);
      foreach ($data as $row) 
      {
        $output['JB_ID'] = $row->JB_ID;
        $output['JB_NAMA'] = $row->JB_NAMA;
      }
      echo json_encode($output);
    }

    public function action()
    {
      if ($this->input->post('ID') == '') {
        $save_data = array(
          'JB_NAMA'     => $this->input->post('JB_NAMA'),
          'JB_STATUS'   => STATUS_ACTIVE
          );
        $this->mjenis_barang->save($save_data);
      } else {
        $updated_data = array(
          'JB_NAMA'     => $this->input->post('JB_NAMA')
        );
        $this->mjenis_barang->update($this->input->post("ID"), $updated_data);
      }
      
    }
}