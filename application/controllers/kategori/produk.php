<?php

class Produk extends MY_Controller {
   //constructor class
    public function __construct() {
      parent::__construct();
      //if(!$this->auth->validate(true)) exit(0);
      //$this->cek_login();
      $this->load->model('mkat_produk');
    //   //$this->breadcrumb->add(lang_value('jnsab_label'), 'jnsab');
    }

    public function index(){
      $data['prod_cat'] = $this->mkat_produk->get('KPR_STATUS = '.STATUS_ACTIVE);
      $this->load->view('base/header');
      $this->load->view('base/menu_admin');
      $this->load->view('kategori/produk', $data);
      $this->load->view('base/footer');
    }

    public function delete($KPR_ID)
    {
      if(!$this->input->is_ajax_request()) show_404();

      if($KPR_ID)
      {
        $this->mkat_produk->delete($KPR_ID);
      }
      else
      {
        ajax_response('failed', 'Gagal');
      }

      ajax_response();
    }

    public function get_by_id()
    {
      $output = array();
      $data = $this->mkat_produk->get('KPR_ID = '.$_POST["KPR_ID"]);
      foreach ($data as $row) 
      {
        $output['KPR_ID'] = $row->KPR_ID;
        $output['KPR_NAMA'] = $row->KPR_NAMA;
      }
      echo json_encode($output);
    }

    public function action()
    {
      if ($this->input->post('ID') == '') {
        $save_data = array(
          'KPR_NAMA'     => $this->input->post('KPR_NAMA'),
          'KPR_STATUS'   => STATUS_ACTIVE
          );
        $this->mkat_produk->save($save_data);
      } else {
        $updated_data = array(
          'KPR_NAMA'     => $this->input->post('KPR_NAMA')
        );
        $this->mkat_produk->update($this->input->post("ID"), $updated_data);
      }
      
    }

}