<?php

  class Posisi_karyawan extends MY_Controller {
   //constructor class
    public function __construct() {
      parent::__construct();
      //if(!$this->auth->validate(true)) exit(0);
      //$this->cek_login();
      $this->load->model('mposisi_karyawan');
      //$this->breadcrumb->add(lang_value('jnsab_label'), 'jnsab');
    }

    public function index(){
      $data['emp_pos'] = $this->mposisi_karyawan->get('KP_STATUS = '.STATUS_ACTIVE);
      $this->load->view('base/header');
      $this->load->view('base/menu_admin');
      $this->load->view('kategori/posisi_karyawan', $data);
      $this->load->view('base/footer');
    }

    public function delete($KP_ID){
      if(!$this->input->is_ajax_request()) show_404();

      if($KP_ID)
      {
        $this->mposisi_karyawan->delete($KP_ID);
      }
      else
      {
        ajax_response('failed', 'Gagal');
      }

      ajax_response();
    }

    public function get_by_id()
    {
      $output = array();
      $data = $this->mposisi_karyawan->get('KP_ID = '.$_POST["KP_ID"]);
      foreach ($data as $row) 
      {
        $output['KP_ID'] = $row->KP_ID;
        $output['KP_NAMA'] = $row->KP_NAMA;
      }
      echo json_encode($output);
    }

    public function action()
    {
      if ($this->input->post('ID') == '') {
        $save_data = array(
          'KP_NAMA'       => $this->input->post('KP_NAMA'),
          'KP_STATUS'    => STATUS_ACTIVE
          );
        $this->mposisi_karyawan->save($save_data);
      } else {
        $updated_data = array(
          'KP_NAMA'       => $this->input->post('KP_NAMA')
        );
        $this->mposisi_karyawan->update($this->input->post("ID"), $updated_data);
      }
      
    }

  }