<?php

  class Bahan extends MY_Controller {
   //constructor class
    public function __construct() {
      parent::__construct();
    //   //if(!$this->auth->validate(true)) exit(0);
    //   //$this->cek_login();
      $this->load->model('mkat_bahan');
    //   //$this->breadcrumb->add(lang_value('jnsab_label'), 'jnsab');
    }

    public function index(){
      $data['kat_bahan'] = $this->mkat_bahan->get('KB_STATUS = '.STATUS_ACTIVE);
      $data['jenis_barang'] = $this->mkat_bahan->get_item('JB_STATUS = '.STATUS_ACTIVE);
      $this->load->view('base/header');
      $this->load->view('base/menu_admin');
      $this->load->view('kategori/bahan', $data);
      $this->load->view('base/footer');
    }

    public function delete($KB_ID)
    {
      if(!$this->input->is_ajax_request()) show_404();

      if($KB_ID)
      {
        $this->mkat_bahan->delete($KB_ID);
      }
      else
      {
        ajax_response('failed', 'Gagal');
      }

      ajax_response();
    }

    public function get_by_id()
    {
      $output = array();
      $data = $this->mkat_bahan->get('KB_ID = '.$_POST["KB_ID"]);
      foreach ($data as $row) 
      {
        $output['KB_ID'] = $row->KB_ID;
        $output['KB_NAMA'] = $row->KB_NAMA;
        $output['JB_ID'] = $row->JB_ID;
      }
      echo json_encode($output);
    }

    public function action()
    {
      if ($this->input->post('ID') == '') {
        $save_data = array(
          'KB_NAMA'      => $this->input->post('KB_NAMA'),
          'JB_ID'      => $this->input->post('JB_ID'),
          'KB_STATUS'    => STATUS_ACTIVE
          );
        $this->mkat_bahan->save($save_data);
      } else {
        $updated_data = array(
          'KB_NAMA'      => $this->input->post('KB_NAMA'),
          'JB_ID'      => $this->input->post('JB_ID')
        );
        $this->mkat_bahan->update($this->input->post("ID"), $updated_data);
      }
      
    }

}