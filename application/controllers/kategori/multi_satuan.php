<?php

  class Multi_satuan extends MY_Controller {
   //constructor class
    public function __construct() {
      parent::__construct();
    //   //if(!$this->auth->validate(true)) exit(0);
    //   //$this->cek_login();
      $this->load->model('mmulti_satuan');
    //   //$this->breadcrumb->add(lang_value('jnsab_label'), 'jnsab');
    }

    public function index(){
      $data['unit_multi'] = $this->mmulti_satuan->get('MS_STATUS = '.STATUS_ACTIVE);
      $data['unit'] = $this->mmulti_satuan->get_unit('ST_STATUS = '.STATUS_ACTIVE);
      $this->load->view('base/header');
      $this->load->view('base/menu_admin');
      $this->load->view('kategori/multi_satuan', $data);
      $this->load->view('base/footer');
    }

    public function delete($MS_ID){
      if(!$this->input->is_ajax_request()) show_404();

      if($MS_ID)
      {
        $this->mmulti_satuan->delete($MS_ID);
      }
      else
      {
        ajax_response('failed', 'Gagal');
      }

      ajax_response();
    }

    public function get_by_id()
    {
      $output = array();
      $data = $this->mmulti_satuan->get('MS_ID = '.$_POST["MS_ID"]);
      foreach ($data as $row) 
      {
        $output['MS_ID'] = $row->MS_ID;
        $output['MS_SATUAN1'] = $row->MS_SATUAN1;
        $output['MS_VALUE1'] = $row->MS_VALUE1;
        $output['MS_SATUAN2'] = $row->MS_SATUAN2;
        $output['MS_VALUE2'] = $row->MS_VALUE2;
      }
      echo json_encode($output);
    }

    public function action()
    {
      if ($this->input->post('ID') == '') {
        $save_data = array(
          'MS_SATUAN1'    => $this->input->post('MS_SATUAN1'),
          'MS_VALUE1'       => $this->input->post('MS_VALUE1'),
          'MS_SATUAN2'        => $this->input->post('MS_SATUAN2'),
          'MS_VALUE2'       => $this->input->post('MS_VALUE2'),
          'MS_STATUS'   => STATUS_ACTIVE
          );
        $this->mmulti_satuan->save($save_data);
      } else {
        $updated_data = array(
          'MS_SATUAN1'     => $this->input->post('MS_SATUAN1'),
          'MS_VALUE1'       => $this->input->post('MS_VALUE1'),
          'MS_SATUAN2'        => $this->input->post('MS_SATUAN2'),
          'MS_VALUE2'       => $this->input->post('MS_VALUE2')
        );
        $this->mmulti_satuan->update($this->input->post("ID"), $updated_data);
      }
      
    }
}