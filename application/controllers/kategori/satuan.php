<?php

class Satuan extends MY_Controller {
   // constructor class
    public function __construct() {
      parent::__construct();
      //if(!$this->auth->validate(true)) exit(0);
      //$this->cek_login();
      $this->load->model('msatuan');
      //$this->breadcrumb->add(lang_value('jnsab_label'), 'jnsab');
    }

    public function index()
    {
      $data['satuan'] = $this->msatuan->get('ST_STATUS = '.STATUS_ACTIVE);
      $this->load->view('base/header');
      $this->load->view('base/menu_admin');
      $this->load->view('kategori/satuan', $data);
      $this->load->view('base/footer');
    }

    public function delete($ST_ID){
      if(!$this->input->is_ajax_request()) show_404();

      if($ST_ID)
      {
        $this->msatuan->delete($ST_ID);
      }
      else
      {
        ajax_response('failed', 'Gagal');
      }

      ajax_response();
    }

    public function get_by_id()
    {
      $output = array();
      $data = $this->msatuan->get('ST_ID = '.$_POST["ST_ID"]);
      foreach ($data as $row) 
      {
        $output['ST_ID'] = $row->ST_ID;
        $output['ST_KODE'] = $row->ST_KODE;
        $output['ST_NAMA'] = $row->ST_NAMA;
      }
      echo json_encode($output);
    }

    public function action()
    {
      if ($this->input->post('ID') == '') {
        $save_data = array(
          'ST_KODE'     => $this->input->post('ST_KODE'),
          'ST_NAMA'     => $this->input->post('ST_NAMA'),
          'ST_STATUS'   => STATUS_ACTIVE
          );
        $this->msatuan->save($save_data);
      } else {
        $updated_data = array(
          'ST_KODE'     => $this->input->post('ST_KODE'),
          'ST_NAMA'     => $this->input->post('ST_NAMA')
        );
        $this->msatuan->update($this->input->post("ID"), $updated_data);
      }
      
    }

}