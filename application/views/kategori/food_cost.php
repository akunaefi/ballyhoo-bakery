<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Biaya Makanan
    </h1>
    <ol class="breadcrumb">
      <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Biaya Makanan</li>
			<li class="active">Daftar Biaya Makanan</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
		<div class="row">
      <div class="col-xs-12">
        <div class="box box-warning">
					<div id="modalInput" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Biaya Makanan</h4>
								</div>
								<div class="modal-body">
									<form method="post" id="add_form" enctype="multipart/form-data">
										<input type="hidden" name="ID" id="ID">
										<div class="form-group">	
											<label>Biaya Makanan</label>
											<input type="text" class="form-control" placeholder="Masukkan biaya makanan" name="FC_NAMA" id="FC_NAMA" required>
										</div>
										<div class="form-group">	
											<label>Persentase</label>
											<input type="text" class="form-control" placeholder="Masukkan persentase biaya makanan" name="FC_VALUE" id="FC_VALUE" required>
										</div>
									</form>
								</div>
								<div class="modal-footer">
									<input type="button" value="Cancel" class="btn btn-default" data-dismiss="modal" />
									<button type="submit" id="action" name="action" onclick="javascript:save('kategori/food_cost/action');" class="btn btn-primary">
										<i class="icon-checkmark-circle2"></i> Simpan
									</button>
								</div>
							</div>
						</div>
					</div>
          <div class="box-header">
            <h3 class="box-title">Daftar Biaya Makanan</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">
						<div class="form-group">
							<button type="button" id="tambah" name="tambah" class="btn btn-primary btn-sm tambah"><i class="glyphicon glyphicon-plus"></i> Tambah</button>
							
						</div>
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Biaya Makanan</th>
									<th>Persentase (%)</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($food_cost as $row): ?>
								<tr>
									<td><?php echo $row->FC_NAMA; ?></td>
									<td><?php echo $row->FC_VALUE; ?></td>
									<td>
										<button type="button" name="edit" id="<?php echo $row->FC_ID; ?>" class="btn btn-warning btn-sm editcost" title="Edit">
											<i class="glyphicon glyphicon-pencil"></i>
										</button>
										<button class="btn btn-danger btn-sm" title="Delete" onclick="javascript:konfirmasi('kategori/food_cost/delete/<?php echo $row->FC_ID; ?>');">
											<i class="glyphicon glyphicon-trash"></i>
										</button>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>                 
							</tfoot>
        		</table>
      		</div>
      		<!-- /.box-body -->
    		</div>
    		<!-- /.box -->
  		</div>
		</div>
    <!-- Main row -->
      
    <!-- /.row (main row) -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->