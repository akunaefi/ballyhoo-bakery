<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Kategori Bahan
    </h1>
    <ol class="breadcrumb">
      <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Kategori Bahan</li>
			<li class="active">Daftar Kategori Bahan</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
		<div class="row">
      <div class="col-xs-12">
        <div class="box box-warning">
					<div id="modalInput" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Kategori Bahan</h4>
								</div>
								<div class="modal-body">
									<form method="post" id="add_form" enctype="multipart/form-data">
										<input type="hidden" id="ID" name="ID">
										<div class="form-group">	
											<label>Nama</label>
											<input type="text" name="KB_NAMA" id="KB_NAMA" class="form-control" placeholder="Masukkan nama kategori produk" required>
										</div>
										<div class="form-group">
											<label>Kategori Jenis Barang</label><br>
											<select class="form-control" name="JB_ID" id="JB_ID" style="width:100%" required>
												<option></option>
												<?php foreach ($jenis_barang as $row): ?>
												<option value="<?php echo $row->JB_ID; ?>"><?php echo $row->JB_NAMA; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</form>
								</div>
								<div class="modal-footer">
									<input type="button" value="Cancel" class="btn btn-default" data-dismiss="modal" />
									<button type="submit" id="action" name="action" onclick="javascript:save('kategori/bahan/action');" class="btn btn-primary">
										<i class="icon-checkmark-circle2"></i> Simpan
									</button>
								</div>
							</div>
						</div>
					</div>
          <div class="box-header">
            <h3 class="box-title">Daftar Kategori Bahan</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">
						<div class="form-group">
							<button type="button" id="tambah" name="tambah" class="btn btn-primary btn-sm tambah"><i class="glyphicon glyphicon-plus"></i> Tambah</button>
							
						</div>
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Kode</th>
									<th>Nama Kategori Bahan</th>
									<th>Kategori Jenis Barang</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($kat_bahan as $row): ?>
								<tr>
									<td><?php echo $row->KB_ID; ?></td>
									<td><?php echo $row->KB_NAMA; ?></td>
									<td><?php echo $row->JB_NAMA; ?></td>
									<td>
										<button type="button" name="edit" id="<?php echo $row->KB_ID; ?>" class="btn btn-warning btn-sm editkatbhn" title="Edit">
											<i class="glyphicon glyphicon-pencil"></i>
										</button>
										<button class="btn btn-danger btn-sm" title="Delete" onclick="javascript:konfirmasi('kategori/bahan/delete/<?php echo $row->KB_ID; ?>');">
											<i class="glyphicon glyphicon-trash"></i>
										</button>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>                 
							</tfoot>
        		</table>
      		</div>
      		<!-- /.box-body -->
    		</div>
    		<!-- /.box -->
  		</div>
		</div>
    <!-- Main row -->
      
    <!-- /.row (main row) -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->