<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  	<section class="content-header">
    	<h1>
      		Posisi Karyawan
    	</h1>
    	<ol class="breadcrumb">
      		<li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
      		<li class="active">Posisi Karyawan</li>
			<li class="active">Daftar Posisi Karyawan</li>
    	</ol>
  	</section>

  	<!-- Main content -->
  	<section class="content">
		<div class="row">
      		<div class="col-xs-12">
        		<div class="box box-warning">
					<div id="modalInput" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Posisi Karyawan</h4>
								</div>
								<div class="modal-body">
									<form method="post" id="add_form" enctype="multipart/form-data">
										<input type="hidden" id="ID" name="ID">
										<div class="form-group hilang">	
											<label>Kode</label>
											<input type="text" class="form-control" name="KP_ID" id="KP_ID" disabled>
										</div>
										<div class="form-group">	
											<label>Nama</label>
											<input type="text" class="form-control" placeholder="Masukkan nama posisi Karyawan" name="KP_NAMA" id="KP_NAMA" required>
										</div>
									</form>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
									<button type="submit" id="action" name="action" onclick="javascript:save('kategori/posisi_karyawan/action');" class="btn btn-primary">
										<i class="icon-checkmark-circle2"></i> Simpan
									</button>
								</div>
							</div>
						</div>
					</div>
          			<div class="box-header">
            			<h3 class="box-title">Daftar Posisi Karyawan</h3>
          			</div>
          			<!-- /.box-header -->
          			<div class="box-body table-responsive">
						
						<div class="form-group">
							<button type="button" id="tambah" name="tambah" class="btn btn-primary btn-sm tambah"><i class="glyphicon glyphicon-plus"></i> Tambah </button>
							
						</div>
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Kode</th>
									<th>Nama Posisi Karyawan</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($emp_pos as $row): ?>
								<tr>
									<td><?php echo $row->KP_ID; ?></td>
									<td><?php echo $row->KP_NAMA; ?></td>
									<td>
										<button type="button" name="edit" id="<?php echo $row->KP_ID; ?>" class="btn btn-warning btn-sm editpos" title="Edit">
											<i class="glyphicon glyphicon-pencil"></i>
										</button>
										<button class="btn btn-danger btn-sm" title="Delete" onclick="javascript:konfirmasi('kategori/posisi_karyawan/delete/<?php echo $row->KP_ID; ?>');">
											<i class="glyphicon glyphicon-trash"></i>
										</button>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>                 
							</tfoot>
        				</table>
      				</div>
      		<!-- /.box-body -->
    			</div>
    		<!-- /.box -->
  			</div>
		</div>
    <!-- Main row -->
      
    <!-- /.row (main row) -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->