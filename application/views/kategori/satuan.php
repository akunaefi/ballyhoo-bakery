<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  	<section class="content-header">
    	<h1>
      		Satuan
    	</h1>
    	<ol class="breadcrumb">
      		<li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
      		<li class="active">Satuan</li>
			<li class="active">Daftar Satuan</li>
    	</ol>
  	</section>

  	<!-- Main content -->
  	<section class="content">
		<div class="row">
      		<div class="col-xs-12">
        		<div class="box box-warning">
					<div id="modalInput" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Satuan</h4>
								</div>
								<div class="modal-body">
									<form method="post" id="add_form" enctype="multipart/form-data">
										<input type="hidden" id="ID" name="ID">
										<div class="form-group">	
											<label>Kode</label>
											<input type="text" name="ST_KODE" id="ST_KODE" class="form-control" placeholder="Masukkan kode satuan" required>
										</div>
										<div class="form-group">	
											<label>Nama</label>
											<input type="text" name="ST_NAMA" id="ST_NAMA" class="form-control" placeholder="Masukkan nama satuan" required>
										</div>
									</form>
								</div>
								<div class="modal-footer">
									<input type="button" value="Cancel" class="btn btn-default" data-dismiss="modal" />
									<button type="submit" id="action" name="action" onclick="javascript:save('kategori/satuan/action');" class="btn btn-primary">
										<i class="icon-checkmark-circle2"></i> Simpan
									</button>
								</div>
							</div>
						</div>
					</div>
          			<div class="box-header">
            			<h3 class="box-title">Daftar Satuan</h3>
          			</div>
          			<!-- /.box-header -->
          			<div class="box-body table-responsive">
						<div class="form-group">
							<button type="button" id="tambah" name="tambah" class="btn btn-primary btn-sm tambah"><i class="glyphicon glyphicon-plus"></i> Tambah</button>
							
						</div>
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Kode</th>
									<th>Nama Satuan</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($satuan as $row): ?>
								<tr>
									<td><?php echo $row->ST_KODE; ?></td>
									<td><?php echo $row->ST_NAMA; ?></td>
									<td>
										<button type="button" name="edit" id="<?php echo $row->ST_ID; ?>" class="btn btn-warning btn-sm editsat" title="Edit">
											<i class="glyphicon glyphicon-pencil"></i>
										</button>
										<button class="btn btn-danger btn-sm" title="Delete" onclick="javascript:konfirmasi('kategori/satuan/delete/<?php echo $row->ST_ID; ?>');">
											<i class="glyphicon glyphicon-trash"></i>
										</button>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>                 
							</tfoot>
        		</table>
      		</div>
      		<!-- /.box-body -->
    		</div>
    		<!-- /.box -->
  		</div>
		</div>
    <!-- Main row -->
      
    <!-- /.row (main row) -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->