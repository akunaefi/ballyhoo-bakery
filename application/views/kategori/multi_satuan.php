<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Multi Unit
    </h1>
    <ol class="breadcrumb">
      <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Multi Unit</li>
			<li class="active">Daftar Multi Unit</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
		<div class="row">
      <div class="col-xs-12">
        <div class="box box-warning">
					<div id="modalInput" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Multi Unit</h4>
								</div>
								<div class="modal-body">
									<form method="post" id="add_form" enctype="multipart/form-data">
										<input type="hidden" id="ID" name="ID">
										<div class="form-group hilang">	
											<label>Kode</label>
											<input type="text" name="MS_ID" id="MS_ID" class="form-control">
										</div>
										<div class="form-group">
											<label>Satuan 1</label><br>
											<select class="form-control" name="MS_SATUAN1" id="MS_SATUAN1" style="width:100%" required>
												<option></option>
												<?php foreach ($unit as $row): ?>
												<option value="<?php echo $row->ST_KODE; ?>"><?php echo $row->ST_NAMA; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
										<div class="form-group">	
											<label>Nilai 1</label>
											<input type="text" name="MS_VALUE1" id="MS_VALUE1" class="form-control" placeholder="Masukkan nilai 1" required>
										</div>
										<div class="form-group">
											<label>Satuan 2</label><br>
											<select class="form-control" name="MS_SATUAN2" id="MS_SATUAN2" style="width:100%" required>
												<option></option>
												<?php foreach ($unit as $row): ?>
												<option value="<?php echo $row->ST_KODE; ?>"><?php echo $row->ST_NAMA; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
										<div class="form-group">	
											<label>Nilai 2</label>
											<input type="text" name="MS_VALUE2" id="MS_VALUE2" class="form-control" placeholder="Masukkan nilai 1" required>
										</div>
									</form>
								</div>
								<div class="modal-footer">
									<input type="button" value="Cancel" class="btn btn-default" data-dismiss="modal" />
									<button type="submit" id="action" name="action" onclick="javascript:save('kategori/multi_satuan/action');" class="btn btn-primary">
										<i class="icon-checkmark-circle2"></i> Simpan
									</button>
								</div>
							</div>
						</div>
					</div>
          <div class="box-header">
            <h3 class="box-title">Daftar Multi Unit</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">
						<div class="form-group">
							<button type="button" id="tambah" name="tambah" class="btn btn-primary btn-sm tambah"><i class="glyphicon glyphicon-plus"></i> Tambah</button>
							
						</div>
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Kode</th>
									<th>Satuan 1</th>
									<th>Nilai 1</th>
									<th>Satuan 2</th>
									<th>Nilai 2</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($unit_multi as $row): ?>
								<tr>
									<td><?php echo $row->MS_ID; ?></td>
									<td><?php echo $row->SATUAN1; ?></td>
									<td><?php echo $row->MS_VALUE1; ?></td>
									<td><?php echo $row->SATUAN2; ?></td>
									<td><?php echo $row->MS_VALUE2; ?></td>
									<td>
										<button type="button" name="edit" id="<?php echo $row->MS_ID; ?>" class="btn btn-warning btn-sm editms" title="Edit">
											<i class="glyphicon glyphicon-pencil"></i>
										</button>
										<button class="btn btn-danger btn-sm" title="Delete" onclick="javascript:konfirmasi('kategori/multi_satuan/delete/<?php echo $row->MS_ID; ?>');">
											<i class="glyphicon glyphicon-trash"></i>
										</button>
									</td>
								</tr>
								<?php endforeach; ?>
								</tr>
							</tbody>
							<tfoot>                 
							</tfoot>
        		</table>
      		</div>
      		<!-- /.box-body -->
    		</div>
    		<!-- /.box -->
  		</div>
		</div>
    <!-- Main row -->
      
    <!-- /.row (main row) -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->