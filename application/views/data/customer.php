<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  	<section class="content-header">
    	<h1>
      		Pelanggan
    	</h1>
    	<ol class="breadcrumb">
      		<li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
      		<li class="active">Pelanggan</li>
			<li class="active">Daftar Pelanggan</li>
    	</ol>
  	</section>

  	<!-- Main content -->
  	<section class="content">
		<div class="row">
      		<div class="col-xs-12">
        		<div class="box box-warning">
					<div id="modalInput" class="modal fade">
						<form method="post" id="add_form" enctype="multipart/form-data">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Tambah Pelanggan</h4>
									</div>
									<div class="modal-body">
										<input type="hidden" name="ID" id="ID">
										<div class="form-group">	
											<label>Nama</label>
											<input type="text" class="form-control" placeholder="Masukkan nama pelanggan" name="CTM_NAMA" id="CTM_NAMA" required>
										</div>
										<div class="form-group">	
											<label>Alamat</label>
											<input type="text" class="form-control" placeholder="Masukkan alamat pelanggan" name="CTM_ALAMAT" id="CTM_ALAMAT" required>
										</div>
										<div class="form-group">	
											<label>Telepon</label>
											<input type="text" class="form-control" placeholder="Masukkan telepon pelanggan" name="CTM_TELP" id="CTM_TELP" required>
										</div>
									</div>
									<div class="modal-footer">
										<input type="button" value="Cancel" class="btn btn-default" data-dismiss="modal" />
										<button type="submit" id="action" name="action" onclick="javascript:save('data/customer/action');" class="btn btn-primary">
											<i class="icon-checkmark-circle2"></i> Simpan
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
          			<div class="box-header">
            			<h3 class="box-title">Daftar Pelanggan</h3>
          			</div>
          			<!-- /.box-header -->
          			<div class="box-body table-responsive">
						<div class="form-group">
							<button type="button" id="tambah" name="tambah" class="btn btn-primary btn-sm tambah"><i class="glyphicon glyphicon-plus"></i> Tambah </button>
							
						</div>
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>ID</th>
									<th>Nama</th>
									<th>Alamat</th>
									<th>Telepon</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($customer as $row): ?>
								<tr>
									<td><?php echo $row->CTM_ID; ?></td>
									<td><?php echo $row->CTM_NAMA; ?></td>
									<td><?php echo $row->CTM_ALAMAT; ?></td>
									<td><?php echo $row->CTM_TELP; ?></td>
									<td>
										<button type="button" name="edit" id="<?php echo $row->CTM_ID; ?>" class="btn btn-warning btn-sm editcustomer" title="Edit">
											<i class="glyphicon glyphicon-pencil"></i>
										</button>
										<button class="btn btn-danger btn-sm" title="Delete" onclick="javascript:konfirmasi('data/customer/delete/<?php echo $row->CTM_ID; ?>');">
											<i class="glyphicon glyphicon-trash"></i>
										</button>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>                 
							</tfoot>
        				</table>
      				</div>
      				<!-- /.box-body -->
    			</div>
    		<!-- /.box -->
  			</div>
		</div>
    <!-- Main row -->
      
    <!-- /.row (main row) -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->