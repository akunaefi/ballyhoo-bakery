<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Gudang
    </h1>
    <ol class="breadcrumb">
      <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Gudang</li>
			<li class="active">Daftar Gudang</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
		<div class="row">
      <div class="col-xs-12">
        <div class="box box-warning">
					<div id="modalInput" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Gudang</h4>
								</div>
								<div class="modal-body">
									<form method="post" id="add_form" enctype="multipart/form-data">
										<input type="hidden" name="ID" id="ID">
										<div class="form-group hilang">	
											<label>Kode</label>
											<input type="text" class="form-control" placeholder="Masukkan kode gudang" name="GD_ID" id="GD_ID" disabled>
										</div>
										<div class="form-group">	
											<label>Nama</label>
											<input type="text" class="form-control" placeholder="Masukkan nama gudang" name="GD_NAMA" id="GD_NAMA" required="required">
										</div>
										<div class="form-group">	
											<label>Alamat</label>
											<input type="text" class="form-control" placeholder="Masukkan alamat gudang" name="GD_ALAMAT" id="GD_ALAMAT" required="required">
										</div>
										<div class="form-group">	
											<label>Telepon</label>
											<input type="text" class="form-control" placeholder="Masukkan telepon gudang" name="GD_TELP" id="GD_TELP" required="required">
										</div>
										<div class="form-group">
											<label>Default</label><br>
											<select class="form-control" name="JB_ID" id="JB_ID" style="width:100%" required>
												<option></option>
												<?php foreach ($item_type as $row): ?>
												<option value="<?php echo $row->JB_ID; ?>"><?php echo $row->JB_NAMA; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</form>
								</div>
								<div class="modal-footer">
									<input type="button" value="Cancel" class="btn btn-default" data-dismiss="modal" />
									<button type="submit" id="action" name="action" onclick="javascript:save('data/gudang/action');" class="btn btn-primary">
										<i class="icon-checkmark-circle2"></i> Simpan
									</button>
								</div>
							</div>
						</div>
					</div>
          <div class="box-header">
            <h3 class="box-title">Daftar Gudang</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">
						<div class="form-group">
							<button type="button" id="tambah" name="tambah" class="btn btn-primary btn-sm tambah"><i class="glyphicon glyphicon-plus"></i> Tambah </button>
							
						</div>
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Kode</th>
									<th>Nama</th>
									<th>Alamat</th>
									<th>Telepon</th>
									<th>Kategori Jenis Barang</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($warehouse as $row): ?>
								<tr>
									<td><?php echo $row->GD_ID; ?></td>
									<td><?php echo $row->GD_NAMA; ?></td>
									<td><?php echo $row->GD_ALAMAT; ?></td>
									<td><?php echo $row->GD_TELP; ?></td>
									<td><?php echo $row->JB_NAMA; ?></td>
									<td>
										<button type="button" name="edit" id="<?php echo $row->GD_ID; ?>" class="btn btn-warning btn-sm editgd" title="Edit">
											<i class="glyphicon glyphicon-pencil"></i>
										</button>
										<button class="btn btn-danger btn-sm" title="Delete" onclick="javascript:konfirmasi('data/gudang/delete/<?php echo $row->GD_ID; ?>');">
											<i class="glyphicon glyphicon-trash"></i>
										</button>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>                 
							</tfoot>
        		</table>
      		</div>
      		<!-- /.box-body -->
    		</div>
    		<!-- /.box -->
  		</div>
		</div>
    <!-- Main row -->
      
    <!-- /.row (main row) -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->