
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
 	<section class="content-header">
    	<h1>
      		Bahan
    	</h1>
    	<ol class="breadcrumb">
      		<li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
      		<li class="active">Bahan</li>
			<li class="active">Daftar Bahan</li>
    	</ol>
  	</section>

  	<div id="modalInput" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Bahan</h4>
				</div>
				<form method="post" id="add_form" enctype="multipart/form-data">
					<div class="modal-body">
						<div class="row">
      						<div class="col-md-12">
								<input type="hidden" name="ID" id="ID">
								<div class="form-group">	
									<label>Nama</label>
									<input type="text" class="form-control" placeholder="Masukkan nama bahan" name="BHN_NAMA" id="BHN_NAMA" required>
								</div>
								<div class="form-group">
									<label>Kategori Bahan</label><br>
									<select class="form-control" name="KB_ID" id="KB_ID" style="width:100%" required>
										<option></option>
										<?php foreach ($kbhn as $ing): ?>
										<option value="<?php echo $ing->JB_ID; ?>"><?php echo $ing->KB_NAMA; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="form-group">
									<label>Gudang</label><br>
									<select class="form-control" name="GD_ID" id="GD_ID" style="width:100%" required>
										<option></option>
										<?php foreach ($gdg as $rec): ?>
										<option value="<?php echo $rec->GD_ID; ?>"><?php echo $rec->GD_NAMA; ?> - <?php echo $rec->JB_NAMA; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="form-group">
									<label>Pemasok</label><br>
									<select class="form-control" name="SP_ID" id="SP_ID" style="width:100%" required>
										<option></option>
										<?php foreach ($sup as $row): ?>
										<option value="<?php echo $row->SP_ID; ?>"><?php echo $row->SP_NAMA; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="form-group">
                					<label>Tanggal Kadaluarsa</label>
                					<div class="input-group date">
                  						<div class="input-group-addon">
                    						<i class="fa fa-calendar"></i>
                  						</div>
                  						<input type="text" data-date-format="yyyy-mm-dd" class="form-control pull-right" id="BHN_EXP" name="BHN_EXP">
                					</div>
                					<!-- /.input group -->
    							</div>
    						</div>
      						<div class="col-md-6">
								<div class="form-group">	
									<label>Stok</label>
									<input type="text" class="form-control" placeholder="Stok bahan" name="BHN_STOK" id="BHN_STOK" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">	
									<label>Stok Minimal</label>
									<input type="text" class="form-control" placeholder="Stok minimal" name="BHN_MINSTOK" id="BHN_MINSTOK" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">	
									<label>Order Minimal</label>
									<input type="text" class="form-control" placeholder="Order minimal" name="BHN_MINORDER" id="BHN_MINORDER" required>
								</div>
							</div>
      						<div class="col-md-6">
								<div class="form-group">
									<label>Satuan</label><br>
									<select class="form-control" name="ST_ID" id="ST_ID" style="width:100%" required>
										<option></option>
										<?php foreach ($sat as $row): ?>
										<option value="<?php echo $row->ST_ID; ?>"><?php echo $row->ST_NAMA; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">	
									<label>Harga Satuan Bahan</label>
									<div class="input-group">
                  						<div class="input-group-addon">
                    						<label>Rp</label>
                  						</div>
                  						<input type="text" class="form-control" placeholder="Masukkan harga satuan" name="BHN_HARGA" id="BHN_HARGA" required>
                					</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="modal-footer">
					<input type="button" value="Cancel" class="btn btn-default" data-dismiss="modal" />
					<button type="submit" id="action" name="action" onclick="javascript:save('data/bahan/action');" class="btn btn-primary">
						<i class="icon-checkmark-circle2"></i> Simpan
					</button>
				</div>
			</div>
		</div>
	</div>

  	<!-- Main content -->
  	<section class="content" id="tabel">
		<div class="row">
      		<div class="col-md-12">
        		<div class="box box-warning">
          			<div class="box-header">
            			<h3 class="box-title">Daftar Bahan</h3>
          			</div>
          			<div class="box-body table-responsive">
						<div class="form-group">
							<button type="button" id="tambah" name="tambah" class="btn btn-primary btn-sm tambah"><i class="glyphicon glyphicon-plus"></i> Tambah</button>
						</div>
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Nama Bahan</th>
									<th>Expire</th>
									<th>Stok</th>
									<th>Gudang</th>
									<th>Last Update</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($bahan as $row): ?>
								<tr>
									<td><?php echo $row->BHN_NAMA; ?></td>
									<td><?php echo $row->BHN_EXP; ?></td>
									<td><?php echo $row->BHN_STOK; ?> <?php echo $row->ST_NAMA; ?></td>
									<td><?php echo $row->GD_NAMA; ?> - <?php echo $row->JB_NAMA; ?></td>
									<td><?php echo $row->BHN_UPDATE; ?></td>
									<td>
										<button type="button" name="edit" id="<?php echo $row->BHN_ID; ?>" class="btn btn-warning btn-sm editbhn" title="Edit">
											<i class="glyphicon glyphicon-pencil"></i>
										</button>
										<button class="btn btn-danger btn-sm" title="Delete" onclick="javascript:konfirmasi('data/bahan/delete/<?php echo $row->BHN_ID; ?>');">
											<i class="glyphicon glyphicon-trash"></i>
										</button>
										<button type="button" name="detail" id="<?php echo $row->BHN_ID; ?>" class="btn btn-info btn-sm detail" title="Detail">
											<i class="glyphicon glyphicon-zoom-in"></i>
										</button>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>
								<tr>
									<th>Nama Bahan</th>
									<th>Expire</th>
									<th>Stok</th>
									<th>Gudang</th>
									<th>Aksi</th>
								</tr>                 
							</tfoot>
        				</table>
      				</div>
      				<!-- /.box-body -->
    			</div>
    			<!-- /.box -->
  			</div>
		</div>
    <!-- Main row -->
      
    <!-- /.row (main row) -->

  	</section>
  	<!-- /.content -->

  	<!-- Detail Bahan -->
  	<section class="content" style="display: none;" id="dispdetail">
  		<form method="post" id="det_form" enctype="multipart/form-data">
		<div class="box box-warning">
			<div class="box-header">
            	<h3 class="box-title">Detail Bahan</h3>
          	</div>
			<div class="box-body">
				<div class="form-group">
					<button type="button" id="kembali" name="kembali" class="btn btn-primary btn-sm kembali"><i class="glyphicon glyphicon-arrow-left"></i> Kembali</button>
				</div>
				<div class="row">
      				<div class="col-md-6">
          				<div class="form-group">
          					<label>Nama Bahan</label>
          					<input type="text" class="form-control" name="BHN_NAMA" id="DBHN_NAMA" disabled>
          				</div>
          				<div class="form-group">
          					<label>Kategori Bahan</label>
          					<input type="text" class="form-control" name="KB_NAMA" id="DKB_NAMA" disabled>
          				</div>
          				<div class="form-group">	
							<label>Harga Satuan Bahan</label>
							<div class="input-group">
                  				<div class="input-group-addon">
                    				<label>Rp</label>
                  				</div>
                  				<input type="text" class="form-control uang" name="DBHN_HARGA" id="DBHN_HARGA" disabled>
                			</div>
          				</div>
          			</div>
          			<div class="col-md-3">
          				<div id="stok" class="form-group">
          					<label>Stok Bahan</label>
          					<input type="text" class="form-control" name="BHN_STOK" id="DBHN_STOK" disabled>
          				</div>
          			</div>
          			<div class="col-md-3">
          				<div id="stok" class="form-group">
          					<label>Stok Minimal</label>
          					<input type="text" class="form-control" name="BHN_MINSTOK" id="DBHN_MINSTOK" disabled>
          				</div>
          			</div>
          			<div class="col-md-6">
          				<div class="form-group">
          					<label>Minimal Order</label>
          					<input type="text" class="form-control" name="BHN_MINORDER" id="DBHN_MINORDER" disabled>
          				</div>
          				<div class="form-group">
          					<label>Tanggal Kadaluarsa</label>
          					<input type="text" class="form-control" name="BHN_EXP" id="DBHN_EXP" disabled>
          				</div>
          			</div>
          		</div>
          	</div>
          	<div class="box-body">
          		<div class="row">
          			<div class="col-md-9">
          				<div class="form-group">
          					<label>Pemasok</label>
          					<input type="text" class="form-control" name="SP_NAMA" id="DSP_NAMA" disabled>
          				</div>
          			</div>
          			<div class="col-md-3">
          				<div class="form-group">
          					<label>Telp. Pemasok</label>
          					<input type="text" class="form-control" name="SP_TELP" id="DSP_TELP" disabled>
          				</div>
          			</div>
          			<div class="col-md-12">
          				<div class="form-group">
          					<label>Alamat Pemasok</label>
          					<textarea class="form-control" name="SP_ALAMAT" id="DSP_ALAMAT" disabled></textarea>
          				</div>
          			</div>
          		</div>
         	</div>
         	<div class="box-body">
          		<div class="row">
          			<div class="col-md-9">
          				<div class="form-group">
          					<label>Gudang</label>
          					<input type="text" class="form-control" name="GD_NAMA" id="DGD_NAMA" disabled>
          				</div>
          			</div>
          			<div class="col-md-3">
          				<div class="form-group">
          					<label>Telp. Gudang</label>
          					<input type="text" class="form-control" name="GD_TELP" id="DGD_TELP" disabled>
          				</div>
          			</div>
          			<div class="col-md-12">
          				<div class="form-group">
          					<label>Alamat Gudang</label>
          					<textarea class="form-control" name="GD_ALAMAT" id="DGD_ALAMAT" disabled></textarea>
          				</div>
          			</div>
          		</div>
         	</div>
        </div>
    </form>
    </section>

</div>
