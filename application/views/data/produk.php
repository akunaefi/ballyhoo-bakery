<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Produk
    </h1>
    <ol class="breadcrumb">
      <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Produk</li>
			<li class="active">Daftar Produk</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
		<div class="row">
      <div class="col-xs-12">
        <div class="box box-warning">
					<div id="modalInput" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Produk</h4>
								</div>
								<div class="modal-body">
									<form method="post" id="add_form" enctype="multipart/form-data">
										<input type="hidden" name="ID" id="ID">
										<div class="form-group">	
											<label>Nama</label>
											<input type="text" class="form-control" placeholder="Masukkan nama produk" name="PRD_NAMA" id="PRD_NAMA" required>
										</div>
										<div class="form-group">
											<label>Kategori Produk</label><br>
											<select class="form-control" name="KPR_ID" id="KPR_ID" style="width:100%" required>
												<option></option>
												<?php foreach ($kp as $ing): ?>
												<option value="<?php echo $ing->KPR_ID; ?>"><?php echo $ing->KPR_NAMA; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
										<div class="form-group">	
											<label>Harga Produk</label>
											<input type="text" class="form-control" placeholder="Masukkan harga produk" name="PRD_HARGA" id="PRD_HARGA" required>
										</div>
										<div class="form-group">	
											<label>Stok</label>
											<input type="text" class="form-control" placeholder="Masukkan stok produk" name="PRD_STOK" id="PRD_STOK" required>
										</div>
										<div class="form-group">
											<label>Gudang</label><br>
											<select class="form-control" name="GD_ID" id="GD_ID" style="width:100%" required>
												<option></option>
												<?php foreach ($gudang as $gd): ?>
												<option value="<?php echo $gd->GD_ID; ?>"><?php echo $gd->GD_NAMA; ?> - <?php echo $gd->JB_NAMA; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</form>
								</div>
								<div class="modal-footer">
									<input type="button" value="Cancel" class="btn btn-default" data-dismiss="modal" />
									<button type="submit" id="action" name="action" onclick="javascript:save('data/produk/action');" class="btn btn-primary">
										<i class="icon-checkmark-circle2"></i> Simpan
									</button>
								</div>
							</div>
						</div>
					</div>
          <div class="box-header">
            <h3 class="box-title">Daftar Produk</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">
						<div class="form-group">
							<button type="button" id="tambah" name="tambah" class="btn btn-primary btn-sm tambah"><i class="glyphicon glyphicon-plus"></i> Tambah</button>
							
						</div>
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Nama Produk</th>
									<th>Kategori Produk</th>
									<th>Harga</th>
									<th>Stok</th>
									<th>Gudang</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($produk as $row): ?>
								<tr>
									<td><?php echo $row->PRD_NAMA; ?></td>
									<td><?php echo $row->KPR_NAMA; ?></td>
									<td><?php echo $row->PRD_HARGA; ?></td>
									<td><?php echo $row->PRD_STOK; ?></td>
									<td><?php echo $row->GD_NAMA; ?></td>
									<td>
										<button type="button" name="edit" id="<?php echo $row->PRD_ID; ?>" class="btn btn-warning btn-sm editprod" title="Edit">
											<i class="glyphicon glyphicon-pencil"></i>
										</button>
										<button class="btn btn-danger btn-sm" title="Delete" onclick="javascript:konfirmasi('data/produk/delete/<?php echo $row->PRD_ID; ?>');">
											<i class="glyphicon glyphicon-trash"></i>
										</button>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>                 
							</tfoot>
        		</table>
      		</div>
      		<!-- /.box-body -->
    		</div>
    		<!-- /.box -->
  		</div>
		</div>
    <!-- Main row -->
      
    <!-- /.row (main row) -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->