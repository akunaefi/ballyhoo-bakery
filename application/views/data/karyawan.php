<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Karyawan
    </h1>
    <ol class="breadcrumb">
      <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Karyawan</li>
			<li class="active">Daftar Karyawan</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
		<div class="row">
      <div class="col-xs-12">
        <div class="box box-warning">
					<div id="modalInput" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Karyawan</h4>
								</div>
								<div class="modal-body">
									<form method="post" id="add_form" enctype="multipart/form-data">
										<input type="hidden" name="ID" id="ID">
										<div class="form-group">	
											<label>Nama</label>
											<input type="text" class="form-control" placeholder="Masukkan nama Karyawan" name="KR_NAMA" id="KR_NAMA" required>
										</div>
										<div class="form-group">	
											<label>Alamat</label>
											<textarea type="text" class="form-control" placeholder="Masukkan alamat Karyawan" name="KR_ALAMAT" id="KR_ALAMAT" required></textarea>
										</div>
										<div class="form-group">	
											<label>Telepon</label>
											<input type="text" class="form-control" placeholder="Masukkan telepon Karyawan" name="KR_TELP" id="KR_TELP" required>
										</div>
										<div class="form-group">
											<label>Posisi</label><br>
											<select class="form-control" name="KP_ID" id="KP_ID" style="width:100%" required>
												<option></option>
												<?php foreach ($kp as $row): ?>
												<option value="<?php echo $row->KP_ID; ?>"><?php echo $row->KP_NAMA; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
										<div class="form-group hilang">	
											<label>Username</label>
											<input type="text" class="form-control" placeholder="Masukkan Username Karyawan" name="KR_USERNAME" id="KR_USERNAME" required>
										</div>
										<div class="form-group hilang">	
											<label>Password</label>
											<input type="password" class="form-control" placeholder="Masukkan Password Karyawan" name="KR_PASSWORD" id="KR_PASSWORD" required>
										</div>
									</form>
								</div>
								<div class="modal-footer">
									<input type="button" value="Cancel" class="btn btn-default" data-dismiss="modal" />
									<button type="submit" id="action" name="action" onclick="javascript:save('data/karyawan/action');" class="btn btn-primary">
										<i class="icon-checkmark-circle2"></i> Simpan
									</button>
								</div>
							</div>
						</div>
					</div>
          <div class="box-header">
            <h3 class="box-title">Daftar Karyawan</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">
						<div class="form-group">
							<button type="button" id="tambah" name="tambah" class="btn btn-primary btn-sm tambah"><i class="glyphicon glyphicon-plus"></i> Tambah</button>
							
						</div>
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Nama</th>
									<th>Alamat</th>
									<th>Telepon</th>
									<th>Posisi</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($karyawan as $row): ?>
								<tr>
									<td><?php echo $row->KR_NAMA; ?></td>
									<td><?php echo $row->KR_ALAMAT; ?></td>
									<td><?php echo $row->KR_TELP; ?></td>
									<td><?php echo $row->KP_NAMA; ?></td>
									<td>
										<button type="button" name="edit" id="<?php echo $row->KR_ID; ?>" class="btn btn-warning btn-sm editemp" title="Edit">
											<i class="glyphicon glyphicon-pencil"></i>
										</button>
										<button class="btn btn-danger btn-sm" title="Delete" onclick="javascript:konfirmasi('data/karyawan/delete/<?php echo $row->KR_ID; ?>');">
											<i class="glyphicon glyphicon-trash"></i>
										</button>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>                 
							</tfoot>
        		</table>
      		</div>
      		<!-- /.box-body -->
    		</div>
    		<!-- /.box -->
  		</div>
		</div>
    <!-- Main row -->
      
    <!-- /.row (main row) -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->