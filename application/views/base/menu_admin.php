<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url();?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Agus</p>
        <a>Manager Produksi</a>
      </div>
    </div>
      
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MENU</li>
      <li>
        <a href="<?php echo base_url();?>dashboard">
          <i class="fa fa-dashboard"></i> 
          <span>Dashboard</span>
        </a>
      </li>
		  <li class="treeview">
        <a href="#">
          <i class="fa fa-list"></i>
          <span>Pesanan</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url();?>order/input_order"><i class="fa fa-pencil"></i> Pesanan Baru</a></li>
          <li><a href="<?php echo base_url();?>order/order_list"><i class="fa fa-list"></i> Daftar Pesanan</a></li>
        </ul>
      </li>
       <li class="treeview">
        <a href="#">
          <i class="fa fa-files-o"></i>
          <span>Resep</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url();?>recipe/input_recipe"><i class="fa fa-pencil"></i> Resep Baru</a></li>
          <li><a href="<?php echo base_url();?>recipe/recipe_list"><i class="fa fa-list"></i> Daftar Resep</a></li>
        </ul>
      </li>
		  <li class="treeview">
        <a href="#">
          <i class="fa fa-cubes"></i>
          <span>Inventory</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url();?>inventory/bahan_masuk"><i class="fa fa-arrow-right"></i> Barang Masuk</a></li>
          <li><a href="<?php echo base_url();?>inventory/bahan_keluar"><i class="fa fa-arrow-left"></i> Barang Keluar</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Master Data</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
			    <li><a href="<?php echo base_url();?>data/customer"><i class="fa fa-circle-o"></i> Data Pelanggan</a></li>
          <li><a href="<?php echo base_url();?>data/karyawan"><i class="fa fa-circle-o"></i> Data Karyawan</a></li>
          <li><a href="<?php echo base_url();?>data/supplier"><i class="fa fa-circle-o"></i> Data Pemasok</a></li>
          <li><a href="<?php echo base_url();?>data/bahan"><i class="fa fa-circle-o"></i> Data Bahan</a></li>
          <li><a href="<?php echo base_url();?>data/produk"><i class="fa fa-circle-o"></i> Data Produk</a></li>
          <li><a href="<?php echo base_url();?>data/gudang"><i class="fa fa-circle-o"></i> Data Gudang</a></li>
			  </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i>
          <span>Master Kategori</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url();?>kategori/food_cost"><i class="fa fa-circle-o"></i> Biaya Makanan</a></li>
          <li><a href="<?php echo base_url();?>kategori/posisi_karyawan"><i class="fa fa-circle-o"></i> Posisi Pegawai</a></li>
          <li><a href="<?php echo base_url();?>kategori/satuan"><i class="fa fa-circle-o"></i> Satuan</a></li>
          <li><a href="<?php echo base_url();?>kategori/multi_satuan"><i class="fa fa-circle-o"></i> Multi Satuan</a></li>
          <li><a href="<?php echo base_url();?>kategori/produk"><i class="fa fa-circle-o"></i> Kategori Produk</a></li>
          <li><a href="<?php echo base_url();?>kategori/bahan"><i class="fa fa-circle-o"></i> Kategori Bahan</a></li>
          <li><a href="<?php echo base_url();?>kategori/jenis_barang"><i class="fa fa-circle-o"></i> Kategori Jenis Barang</a></li>
          <li><a href="<?php echo base_url();?>kategori/resep"><i class="fa fa-circle-o"></i> Kategori Resep</a></li>
			  </ul>
      </li>
      <li>
        <a href="<?php echo base_url();?>help/user_guide">
          <i class="fa fa-question"></i> 
          <span>Bantuan</span>
        </a>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>