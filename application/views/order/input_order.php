<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pesanan
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pesanan</li>
				<li class="active">Pesanan Baru</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
			<div class="box box-warning">
				<div class="box-body table-responsive">
					<div class="modal fade" id="modal-order">
					<div class="modal-dialog">
						<div class="modal-content">
							<!-- Modal Header -->
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Konfirmasi</h4>
							</div>
							<!-- Modal body -->
							<div class="modal-body">
								Melanjutkan pemesanan?
							</div>
							<!-- Modal footer -->
							<div class="modal-footer">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
								<a href="<?php echo base_url();?>order/order_detail">
									<button type="button" class="btn btn-success">Ya</button>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="box-header with-border">
					<h3 class="box-title">Pesanan Baru</h3>
				</div>

				<div class="box-body">
					<form action="<?=base_url()?>buatlaporan/save" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nama</label>
							<select class="form-control" required="required">
							<option></option>
							<option>Gusti Syailendra</option>
							<option>Andika</option>
							<option>Anik</option>
							</select>
						</div>
						<div class="form-group">
							<label>Produk</label>
							<select class="form-control" required="required">
							<option></option>
							<option>Kue Bolu</option>
							<option>Roti Sosis</option>
							<option>Roti Abon</option>
							</select>
						</div>
						<div class="form-group">
							<label>Jumlah</label>
							<input type="text" class="form-control" placeholder="Masukkan Jumlah Produk" name="quantity" required="required">
						</div>
						<button type="submit" class="btn btn-warning" name="button"><i class="glyphicon glyphicon-plus"></i> Tambah</button>
						<button type="submit" class="btn" name="button"></i> Batal</button>
					</form>
				</div>

				<div class="form-group">
					<div class="callout callout-danger">
					</div>
				</div>

				<div class="box-body" style="border-bottom:1px solid">
					<table class="table table-condensed" style="max-width:60%; float:left">
						<tbody>
							<tr>
								<th>No</th>
								<td>001</td>
							</tr>
							<tr>
								<th>Nama</th>
								<td>Aditya Alfin Kurniawan</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-condensed" style="max-width:100px; float:right;">
						<tbody>
							<tr>
								<th>Tanggal</th>	
							</tr>
							<tr>
								<td>12-08-2018</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="box-body" style="overflow-x:scroll">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th style="text-align:center">No</th>
								<th style="text-align:center">Produk</th>
								<th style="text-align:center">Jumlah</th>
								<th style="text-align:center">Harga Satuan</th>
								<th style="text-align:center">Harga</th>
								<th style="text-align:center">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="width:10px; text-align:center">1</td>
								<td style="width:500px">Roti Bolu</td>
								<td style="width:100px; text-align:center">10</td>
								<td style="width:300px; text-align:center">Rp. 100.000,-</td>
								<td style="width:300px; text-align:center">Rp. 1.000.000,-</td>
								<td>
									<button class="btn btn-danger btn-sm" title="Delete" data-toggle="modal" data-target="#modal-delete">
										<i class="glyphicon glyphicon-trash"></i>
									</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="box-body">
					<div class="form-group">
						<div class="callout callout-danger">
							<h4 style="float:left">Total Harga</h4>
							<h4 style="text-align:right">Rp. 1.850.000,-</h4>
						</div>
					</div>
					<button style="width:100%" type="submit" class="btn btn-warning btn-lg" name="button" data-toggle="modal" data-target="#modal-order">
						<i class="glyphicon glyphicon-check"></i> 
						Pesan
					</button>
				</div>

			</div>
	  
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->