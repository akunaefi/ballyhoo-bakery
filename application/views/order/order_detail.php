<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pesanan
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pesanan</li>
		<li class="active">Detail Pesanan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
			<div class="box box-warning">
				<div class="box-header with-border">
					<h3 class="box-title">Detail Pesanan</h3>
				</div>
				
				<div class="box-body table-responsive">
							<div class="form-group">
								<a href="<?php echo base_url();?>order/recipe">
									<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-input">Resep</button>
								</a>
								<a href="<?php echo base_url();?>order/order_list">
									<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-input">Selesai</button>
								</a>
								<button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete">Hapus</button>
								<div style="float:right">
									<button style="background-color:red" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-input"></i> PDF </button>
									<button style="background-color:green" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-input"></i> Excel </button>
								</div>
							</div>

				<div class="form-group">
					<div class="callout callout-danger">
					</div>
				</div>

				<div class="box-body" style="border-bottom:1px solid">
					<table class="table table-condensed" style="max-width:60%; float:left">
						<tbody>
							<tr>
								<th>No</th>
								<td>001</td>
							</tr>
							<tr>
								<th>Nama</th>
								<td>Aditya Alfin Kurniawan</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-condensed" style="max-width:100px; float:right;">
						<tbody>
							<tr>
								<th>Tanggal</th>	
							</tr>
							<tr>
								<td>12-08-2018</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="box-body" style="overflow-x:scroll">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th style="text-align:center">No</th>
								<th style="text-align:center">Produk</th>
								<th style="text-align:center">Jumlah</th>
								<th style="text-align:center">Harga Satuan</th>
								<th style="text-align:center">Harga</th>
								<th style="text-align:center">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="width:10px; text-align:center">1.</td>
								<td style="width:300px">Roti Bolu</td>
								<td style="width:200px; text-align:center">10</td>
								<td style="width:200px; text-align:center">Rp. 100.000,-</td>
								<td style="width:200px; text-align:center">Rp. 1.000.000,-</td>
								<td style="width:150px; text-align:center">
									<a href="<?php echo base_url();?>order/order_recipe">
										<button class="btn btn-primary btn-sm" title="Detail" data-toggle="modal" data-target="#modal-order">
											<i class="glyphicon glyphicon-file"></i>
										</button>
									</a>
									<a href="<?php echo base_url();?>order/order_edit">
										<button class="btn btn-warning btn-sm" title="Edit" data-toggle="modal" data-target="#modal-order">
											<i class="glyphicon glyphicon-pencil"></i>
										</button>
									</a>
									<button class="btn btn-danger btn-sm" title="delete" data-toggle="modal" data-target="#modal-delete-item">
										<i class="glyphicon glyphicon-trash"></i>
									</button>
									</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="box-body">
					<div class="form-group">
						<div class="callout callout-danger">
							<h4 style="float:left">Total Harga</h4>
							<h4 style="text-align:right">Rp. 1.850.000,-</h4>
						</div>
					</div>
				</div>

			</div>
	  
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->