<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Resep
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Resep</li>
		<li class="active">Daftar Resep</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	    <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning" style="padding:10px">
            <div class="box-header with-border" style="padding:10px">
              <h3 class="box-title">Daftar Resep</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <div class="form-group" style="float:left">
								<a href="<?php echo base_url();?>recipe/input_recipe">
									<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-input">Baru</button>
								</a>
							</div>
              <div class="form-group" style="float:right">
								<button style="background-color:red" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-input"></i> PDF </button>
								<button style="background-color:green" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-input"></i> Excel </button>
              </div>
              <div class="form-group" style="float:right">
                <div style="float:right; margin-right:20px">
                  <select class="form-control" name="unit">
                    <option>- Rekap -</option>
                    <option>Harian</option>
                    <option>Bualanan</option>
                    <option>Tahunan</option>
                  </select>
                </div>
                <div style="float:right; margin-right:20px">
                  <form>
                    <input class="form-control" type="date" name="date">
                  </form>
                </div>
              </div>

              <div class="form-group" style="clear:right">
                <div class="callout callout-danger">
                </div>
              </div>

              <div class="box-body" style="overflow-x:scroll; width:100%">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="text-align:center">Kode</th>
                      <th style="text-align:center">Nama Resep</th>
                      <th style="text-align:center">Kategori Resep</th>
                      <th style="text-align:center">Tgl</th>
                      <th style="text-align:center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td style="width:100px; text-align:center">78</td>
                      <td style="width:300px;">Roti</td>
                      <td style="width:300px;">Roti Manis</td>
                      <td style="width:100px; text-align:center">12-10-2018</td>
                      <td style="width:100px; text-align:center">
                        <a href="<?php echo base_url();?>recipe/recipe_detail">
                          <button class="btn btn-primary btn-sm" title="Next Recipe" data-toggle="modal" data-target="#modal-next">
                            <i>Detail</i>
                          </button>
                        </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
	    </div>
      <!-- Main row -->
      
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->