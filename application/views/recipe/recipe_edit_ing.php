<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Resep
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Resep</li>
				<li class="active">Resep Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
			<div class="box box-warning">
				<div class="box-body table-responsive">
					<div class="modal fade" id="modal-edit">
					<div class="modal-dialog">
						<div class="modal-content">
							<!-- Modal Header -->
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Konfirmasi</h4>
							</div>
							<!-- Modal body -->
							<div class="modal-body">
								Melanjutkan mengedit?
							</div>
							<!-- Modal footer -->
							<div class="modal-footer">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
								<a href="<?php echo base_url();?>recipe/recipe_detail">
									<button type="button" class="btn btn-success">Ya</button>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="box-header with-border">
					<h3 class="box-title">Resep Edit</h3>
				</div>

				<div class="box-body">
					<form action="<?=base_url()?>buatlaporan/save" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Kategori Bahan</label>
							<select class="form-control" required="required">
							<option></option>
							<option>Gusti Syailendra</option>
							<option>Andika</option>
							<option>Anik</option>
							</select>
						</div>
						<div class="form-group">
							<label>Nama Bahan</label>
							<input type="text" class="form-control" placeholder="Masukkan nama bahan" name="quantity" required="required">
						</div>
						<div class="form-group">
							<label>Kuantitas</label>
							<input type="text" class="form-control" placeholder="Masukkan kuantitas" name="quantity" required="required">
						</div>
						<div class="form-group">
							<label>Satuan</label>
							<select class="form-control" required="required">
							<option></option>
							<option>Gusti Syailendra</option>
							<option>Andika</option>
							<option>Anik</option>
							</select>
						</div>
						<div class="form-group">
							<label>Yield</label>
							<input type="text" class="form-control" placeholder="Masukkan yield" name="quantity" required="required">
						</div>
						<button type="submit" class="btn btn-warning" name="button" data-toggle="modal" data-target="#modal-edit">
							<i class="glyphicon glyphicon-pencil"></i> 
							Edit
							</button>
						<button type="submit" class="btn" name="button"></i> Batal</button>
					</form>
				</div>

			</div>
	  
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->