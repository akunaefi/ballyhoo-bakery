<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Resep
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Resep</li>
		<li class="active">Detail Resep</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	    <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning" style="padding:10px">
            <div class="box-header with-border" style="padding:10px">
              <h3 class="box-title">Detail Resep</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <div class="modal fade" id="modal-delete">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Konfirmasi</h4>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                      Melanjutkan menghapus?
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                      <a href="<?php echo base_url();?>recipe/recipe_detail">
                        <button type="button" class="btn btn-success">Ya</button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group" style="float:left;">
								<a href="<?php echo base_url();?>recipe/recipe_list">
									<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-input">Selesai</button>
								</a>
							</div>
              <div class="form-group" style="float:left; margin-left:3px">
								<a href="<?php echo base_url();?>recipe/recipe_edit">
									<button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modal-input">Edit</button>
								</a>
							</div>
              <div class="form-group" style="float:left; margin-left:3px">
								<button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete">Hapus</button>
							</div>
              <div class="form-group" style="float:right">
								<button style="background-color:red" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-input"></i> PDF </button>
								<button style="background-color:green" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-input"></i> Excel </button>
              </div>

              <div class="form-group" style="clear:right">
                <div class="callout callout-danger">
                </div>
              </div>

              <div class="box-body" style="border-bottom:1px solid">
                <table class="table table-condensed" style="max-width:50%; float:left">
                  <tbody>
                    <tr>
                      <th style="border-bottom:1px solid">Kode</th>
                      <td style="border-bottom:1px solid">001</td>
                    </tr>
                    <tr>
                      <th>Nama Resep</th>
                      <td>Roti Manis A</td>
                    </tr>
                    <tr>
                      <th>Kategori Resep</th>
                      <td>Roti Manis</td>
                    </tr>
                    <tr>
                      <th>Jumlah Hasil Resep</th>
                      <td>4050</td>
                    </tr>
                    <tr>
                      <th>Satuan Hasil Resep</th>
                      <td>gram</td>
                    </tr>
                  </tbody>
                </table>
                <table class="table table-condensed" style="max-width:50%; float:right;">
                  <tbody>
                    <tr>
                      <th style="border-bottom:1px solid">Tanggal</th>
                      <td style="border-bottom:1px solid">12-08-2018</td>	
                    </tr>
                    <tr>
                      <th>Biaya/Resep</th>
                      <td>70.000</td>	
                    </tr>
                    <tr>
                      <th>Biaya/Hasil Resep</th>
                      <td>70.000</td>	
                    </tr>
                    <tr>
                      <th>Food Cost</th>
                      <td>0%</td>	
                    </tr>
                    <tr>
                      <th>Beban Pokok Produksi</th>
                      <td>70.000</td>	
                    </tr>
                    <tr>
                      <th>Harga Jual</th>
                      <td>75.000</td>	
                    </tr>
                  </tbody>
                </table>
              </div>

              <div class="box-body" style="overflow-x:scroll; width:100%">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style="text-align:center">No</th>
                      <th style="text-align:center">Kategori Bahan</th>
                      <th style="text-align:center">Nama Bahan</th>
                      <th style="text-align:center">Q</th>
                      <th style="text-align:center">Satuan</th>
                      <th style="text-align:center">Yield</th>
                      <th style="text-align:center">Total Q</th>
                      <th style="text-align:center">Rp/Q</th>
                      <th style="text-align:center">Total</th>
                      <th style="text-align:center">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td style="width:10px; text-align:center">1</td>
                      <td style="width:300px">Premix Roti Bolu</td>
                      <td style="width:300px;">Roti Bolu A</td>
                      <td style="width:100px; text-align:center">100</td>
                      <td style="width:100px; text-align:center">Kilo gram</td>
                      <td style="width:10px; text-align:center">10%</td>
                      <td style="width:100px; text-align:center">110</td>
                      <td style="width:100px; text-align:center">50.000</td>
                      <td style="width:100px; text-align:center">70.000</td>
                      <td>
                        <a href="<?php echo base_url();?>recipe/recipe_edit_ing">
                          <button class="btn btn-warning btn-sm" title="Edit" data-toggle="modal" data-target="#modal-edit">
                            <i class="glyphicon glyphicon-pencil"></i>
                          </button>
                        </a>
                        <button class="btn btn-danger btn-sm" title="Delete" data-toggle="modal" data-target="#modal-delete">
                          <i class="glyphicon glyphicon-trash"></i>
                        </button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
	    </div>
      <!-- Main row -->
      
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->