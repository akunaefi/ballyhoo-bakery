
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
 	<section class="content-header">
    	<h1>
      		Bahan Keluar
    	</h1>
    	<ol class="breadcrumb">
      		<li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
      		<li class="active">Bahan Keluar</li>
			<li class="active">Daftar Bahan Keluar</li>
    	</ol>
  	</section>

  	<div id="modalInput" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Bahan Keluar</h4>
				</div>
				<form method="post" id="add_form" enctype="multipart/form-data">
					<div class="modal-body">
						<div class="row">
      						<div class="col-md-12">
								<input type="hidden" name="BHN_ID" id="BHN_ID">
								<input type="hidden" name="GD_ID" id="GD_ID">
								<div class="form-group">	
									<label>Nama Bahan</label>
									<input type="text" class="form-control" name="BHN_NAMA" id="BHN_NAMA" disabled>
								</div>
								<div class="form-group">	
									<label>Gudang</label>
									<input type="text" class="form-control" name="GD_NAMA" id="GD_NAMA" disabled>
								</div>
								<div class="form-group">	
									<label>Supplier</label>
									<input type="text" class="form-control" name="SP_NAMA" id="SP_NAMA" disabled>
								</div>
							</div>
							<div class="col-md-8">
								<div class="form-group">	
									<label>Stok</label>
									<input type="text" class="form-control" name="BHN_STOK" id="BHN_STOK" disabled>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group hiden">	
									<label>Satuan</label><br>
									<input type="text" class="form-control" name="ST_NAMA" id="ST_NAMA" disabled>
								</div>
							</div>
							<div class="col-md-8">
								<div class="form-group">	
									<label>Quantity</label>
									<input type="text" class="form-control" placeholder="Quantity bahan" name="BK_QTY" id="BK_QTY" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group hiden">	
									<label>Satuan</label><br>
									<input type="text" class="form-control" name="ST_NAMA1" id="ST_NAMA1" disabled>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="modal-footer">
					<input type="button" value="Cancel" class="btn btn-default" data-dismiss="modal" />
					<button type="submit" id="action" name="action" onclick="javascript:save('inventory/bahan_keluar/action');" class="btn btn-primary">
						<i class="icon-checkmark-circle2"></i> Simpan
					</button>
				</div>
			</div>
		</div>
	</div>

  	<!-- Main content -->
  	<section class="content" id="tabel">
		<div class="row">
      		<div class="col-md-12">
        		<div class="box box-warning">
          			<div class="box-header">
            			<h3 class="box-title">Daftar Bahan</h3>
          			</div>
          			<div class="box-body table-responsive">
						<div class="form-group">
							<button type="button" class="btn btn-primary btn-sm barang"><i class="glyphicon glyphicon-plus"></i> Tambah</button>
							
						</div>
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Nama Bahan</th>
									<th>Tanggal Keluar</th>
									<th>Quantity</th>
									<th>Gudang</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($bahan_keluar as $row): ?>
								<tr>
									<td><?php echo $row->BHN_NAMA; ?></td> 
									<td><?php echo $row->BK_TGL; ?></td>
									<td><?php echo $row->BK_QTY; ?></td>
									<td><?php echo $row->GD_NAMA; ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>
								<tr>
									<th>Nama Bahan</th>
									<th>Tanggal Keluar</th>
									<th>Quantity</th>
									<th>Gudang</th>
								</tr>                 
							</tfoot>
        				</table>
      				</div>
      				<!-- /.box-body -->
    			</div>
    			<!-- /.box -->
  			</div>
		</div>
    <!-- Main row -->
      
    <!-- /.row (main row) -->

  	</section>
  	<!-- /.content -->

  	<section class="content" id="barang" style="display: none;">
		<div class="row">
      		<div class="col-md-12">
        		<div class="box box-warning">
          			<div class="box-header">
            			<h3 class="box-title">Daftar Bahan</h3>
          			</div>
          			<div class="box-body table-responsive">
						<div class="form-group">
							<button type="button" class="btn btn-primary btn-sm keluar"><i class="glyphicon glyphicon-arrow-left"></i> Kembali</button>
						</div>
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Nama Bahan</th>
									<th>Expire</th>
									<th>Stok</th>
									<th>Gudang</th>
									<th>Last Update</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($bahan as $row): ?>
								<tr>
									<td><?php echo $row->BHN_NAMA; ?></td>
									<td><?php echo $row->BHN_EXP; ?></td>
									<td><?php echo $row->BHN_STOK; ?> <?php echo $row->ST_NAMA; ?></td>
									<td><?php echo $row->GD_NAMA; ?> - <?php echo $row->JB_NAMA; ?></td>
									<td><?php echo $row->BHN_UPDATE; ?></td>
									<td>
										<button type="button" name="ambil" id="<?php echo $row->BHN_ID; ?>" class="btn btn-warning btn-sm ambil" title="Ambil">
											Ambil
										</button>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>
								<tr>
									<th>Nama Bahan</th>
									<th>Expire</th>
									<th>Stok</th>
									<th>Gudang</th>
									<th>Aksi</th>
								</tr>                 
							</tfoot>
        				</table>
      				</div>
      				<!-- /.box-body -->
    			</div>
    			<!-- /.box -->
  			</div>
		</div>
    <!-- Main row -->
      
    <!-- /.row (main row) -->

  	</section>

  	

</div>
