<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dasboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dasboard</li>
		<li class="active">Data Kategori Barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	  <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">Daftar Data Kategori Barang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
			  <div class="form-group">
				<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-input"></i> Cetak </button>
				<button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modal-input"></i> Harian </button>
				<button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modal-input"></i> Mingguan </button>
				<button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modal-input"></i> Bulanan </button>
				<button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modal-input"></i> Tahunan </button>
			  </div>
              <table id="example1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Nama</th>
						<th>Tanggal Pesan</th>
						<th>Jam Pesan</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>78</td>
						<td>Amin</td>
						<td>18-04-2018</td>
						<td>08:30:00</td>
						<td>
							<a href="<?php echo base_url();?>detail_recipe">
								<button class="btn btn-primary btn-sm" title="Next Recipe" data-toggle="modal" data-target="#modal-next">
									<i>Resep</i>
								</button>
							</a>
						</td>
					</tr>
                </tbody>
				<tfoot>                 
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
	  </div>
      <!-- Main row -->
      
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->