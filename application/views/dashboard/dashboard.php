  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3>150</h3>

					<p>Pesanan Baru</p>
				</div>
				<div class="icon">
					<i class="ion ion-bag"></i>
				</div>
				<!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
			</div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3>100</h3>

					<p>Pelanggan Baru</p>
				</div>
				<div class="icon">
					<i class="ion ion-person-add"></i>
				</div>
				<!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
			</div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-green">
				<div class="inner">
					<h3>65</h3>

					<p>Barang Masuk</p>
				</div>
				<div class="icon">
					<i class="ion ion-paper-airplane"></i>
				</div>
				<!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
			</div>
        </div>
        <!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-red">
				<div class="inner">
					<h3>100</h3>

					<p>Barang Keluar</p>
				</div>
				<div class="icon">
					<i class="ion ion-paper-airplane"></i>
				</div>
            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
			</div>
        </div>
        <!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-blue">
				<div class="inner">
					<h3>65</h3>

					<p>Stok Minimum</p>
				</div>
				<div class="icon">
					<i class="ion ion-bag"></i>
				</div>
            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
			</div>
        </div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<a href="<?php echo base_url();?>ingredient_category">
				<div class="small-box bg-orange">
					<div class="inner">
						<h3>65</h3>

						<p>Kategori Barang</p>
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
				<!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
				</div>
			</a>
        </div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-lime">
				<div class="inner">
					<h3>65</h3>

					<p>Kategori Jenis Barang</p>
				</div>
				<div class="icon">
					<i class="ion ion-bag"></i>
				</div>
            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
			</div>
        </div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-black">
				<div class="inner">
					<h3>65</h3>

					<p>Saldo Akhir Barang</p>
				</div>
				<div class="icon">
					<i class="ion ion-paper-airplane"></i>
				</div>
            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
			</div>
        </div>
      </div>
      <!-- /.row -->
    
      <!-- Main row -->
      
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
