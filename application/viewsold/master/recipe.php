<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Recipe
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Recipe</li>
		<li class="active">List Recipe</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	  <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
			<div id="modal-input" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Recipe</h4>
						</div>
						<div class="modal-body">
							<form action="#" method="post" enctype="multipart/form-data">
								<div class="form-group">	
									<label>Recipe Name</label>
									<input type="text" class="form-control" placeholder="Masukkan Nama Resep" name="LPR_SUBJECT" id="LPR_SUBJECT" required>
								</div>
								<div class="form-group">	
									<label>Recipe Type</label>
									<input type="text" class="form-control" placeholder="Masukkan Jenis Resep" name="LPR_SUBJECT" id="LPR_SUBJECT" required>
								</div>
								<div class="form-group">	
									<label>Recipe Ingredients</label>
									<table>
										<td style="width:100%">
											<input type="text" class="form-control" placeholder="Masukkan Bahan Resep" name="LPR_SUBJECT" id="LPR_SUBJECT" required>
										</td>
										<td>
											<button style="margin-left:20px" type="submit" class="btn btn-primary"><i class="icon-checkmark-circle2"></i> Add</button>
										</td>
									</table>
								</div>
								<div class="form-group">
									<label>Recipe Description</label>
									<textarea class="form-control" placeholder="Masukkan Deskripsi" name="LPR_DESKRIPSI" id="LPR_DESKRIPSI" required></textarea>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary"><i class="icon-checkmark-circle2"></i> Save</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-header">
              <h3 class="box-title">List Recipe</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
			  <div class="modal fade" id="modal-delete">
				<div class="modal-dialog">
					<div class="modal-content">
						<!-- Modal Header -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Delete</h4>
						</div>
						<!-- Modal body -->
						<div class="modal-body">
							Are you sure want to delete "Roti bolu" Recipe ?
						</div>
						<!-- Modal footer -->
						<div class="modal-footer">
							<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">Delete</button>
						</div>
					</div>
				</div>
			  </div>
			  <div class="modal fade" id="modal-print">
				<div class="modal-dialog">
				<div id="printThis">
					<div class="modal-content">
						<!-- Modal Header -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Print Recipe</h4>
						</div>
						<!-- Modal body -->
						<div id="Print" class="modal-body">
							Recipe Name		: Roti Bolu<br />
							Recipe Type		: Roti Basah<br />
							Recipe Ingredients<br />
							Terigu		1 kg<br />
							Telur		20 butir<br />
							Mentega		0.5 kg
						</div>
						<!-- Modal footer -->
						<div class="modal-footer">
							<a href="javascript:printPage()">
								<button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-print"></i> Print</button>
							</a>
						</div>
					</div>
				</div>
				</div>
			  </div>
			  <div class="form-group">
				<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-input"><i class="glyphicon glyphicon-plus"></i> Add </button>
			  </div>
              <table id="example1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Recipe Name</th>
						<th>Recipe Type</th>
						<th>Recipe Ingredients</th>
						<th>Recipe Description</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Roti Bolu</td>
						<td>Roti Basah</td>
						<td>Terigu, Telur, Mentega</td>
						<td>Terigu 1 kg, telur 20 butir, Mentega ...</td>
						<td>
							<button class="btn btn-warning btn-sm" title="Edit Recipe" data-toggle="modal" data-target="#modal-input">
								<i class="glyphicon glyphicon-pencil"></i>
							</button>
							<button class="btn btn-danger btn-sm" title="Delete Recipe" data-toggle="modal" data-target="#modal-delete">
								<i class="glyphicon glyphicon-trash"></i>
							</button>
							<button class="btn btn-primary btn-sm" title="Print Recipe" data-toggle="modal" data-target="#modal-print">
								<i class="glyphicon glyphicon-print"></i>
							</button>
						</td>
					</tr>
					<tr>
						<td>Roti keju</td>
						<td>Roti kering</td>
						<td>Keju, Gula, Telur</td>
						<td>Keju 1 kg, Gula 20 kg, Telur ...</td>
						<td>
							<button class="btn btn-warning btn-sm" title="Edit Recipe" data-toggle="modal" data-target="#modal-input">
								<i class="glyphicon glyphicon-pencil"></i>
							</button>
							<button class="btn btn-danger btn-sm" title="Delete Recipe" data-toggle="modal" data-target="#modal-delete">
								<i class="glyphicon glyphicon-trash"></i>
							</button>
							<button class="btn btn-primary btn-sm" title="Print Recipe" data-toggle="modal" data-target="#modal-print">
								<i class="glyphicon glyphicon-print"></i>
							</button>
						</td>
					</tr>
                </tbody>
				<tfoot>                 
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
	  </div>
      <!-- Main row -->
      
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->