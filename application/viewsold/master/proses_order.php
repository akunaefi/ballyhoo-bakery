<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Order
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Order</li>
		<li class="active">List Process Order</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	  <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">List Process Order</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
			  <div class="modal fade" id="modal-next">
				<div class="modal-dialog">
					<div class="modal-content">
						<!-- Modal Header -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Next</h4>
						</div>
						<!-- Modal body -->
						<div class="modal-body">
							Are you sure want to next step ?
						</div>
						<!-- Modal footer -->
						<div class="modal-footer">
							<button type="button" class="btn btn-warning btn-sm" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Confirm</button>
						</div>
					</div>
				</div>
			  </div>
              <table id="example1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Order ID</th>
						<th>Customer Name</th>
						<th>Time Process</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>78</td>
						<td>Amin</td>
						<td>18-04-2018 08:20:30</td>
						<td>
							<span class="btn btn-warning btn-sm">Production</span>
						</td>
						<td>
							<button class="btn btn-primary btn-sm" title="Next Recipe" data-toggle="modal" data-target="#modal-next">
								<i>Next</i>
							</button>
						</td>
					</tr>
					<tr>
						<td>78</td>
						<td>Amin</td>
						<td>18-04-2018 08:20:30</td>
						<td>
							<span class="btn btn-warning btn-sm">Production</span>
						</td>
						<td>
							<button class="btn btn-primary btn-sm" title="Next Process" data-toggle="modal" data-target="#modal-next">
								<i>Next</i>
							</button>
						</td>
					</tr>
                </tbody>
				<tfoot>                 
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
	  </div>
      <!-- Main row -->
      
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->