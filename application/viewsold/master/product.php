<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Product
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Product</li>
		<li class="active">List Product</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	  <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
			<div id="modal-Input" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Product</h4>
						</div>
						<div class="modal-body">
							<form action="#" method="post" enctype="multipart/form-data">
								<div class="form-group">	
									<label>Product Name</label>
									<input type="text" class="form-control" placeholder="Masukkan Nama Produk" name="LPR_SUBJECT" id="LPR_SUBJECT" required>
								</div>
								<div class="form-group">	
									<label>Recipe Name</label>
									<input type="text" class="form-control" placeholder="Masukkan Nama Resep" name="LPR_SUBJECT" id="LPR_SUBJECT" required>
								</div>
								<div class="form-group">	
									<label>Product Type</label>
									<input type="text" class="form-control" placeholder="Masukkan Jenis Produk" name="LPR_SUBJECT" id="LPR_SUBJECT" required>
								</div>
								<div class="form-group">	
									<label>Cost</label>
									<input type="text" class="form-control" placeholder="Masukkan Harga Produk" name="LPR_SUBJECT" id="LPR_SUBJECT" required>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary"><i class="icon-checkmark-circle2"></i> Simpan</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-header">
              <h3 class="box-title">List Product</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
			  <div class="modal fade" id="modal-delete">
				<div class="modal-dialog">
					<div class="modal-content">
						<!-- Modal Header -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Delete</h4>
						</div>
						<!-- Modal body -->
						<div class="modal-body">
							Are you sure want to delete product "Roti bolu" ?
						</div>
						<!-- Modal footer -->
						<div class="modal-footer">
							<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">Delete</button>
						</div>
					</div>
				</div>
			  </div>
			  <div class="form-group">
				<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-Input"><i class="glyphicon glyphicon-plus"></i> Add </button>
			  </div>
              <table id="example1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Product ID</th>
						<th>Product Name</th>
						<th>Product Type</th>
						<th>Recipe Name</th>
						<th>Cost</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>hy78</td>
						<td>Roti Bolu</td>
						<td>Roti Basah</td>
						<td>Terigu, Telur, Mentega</td>
						<td>Rp 30.000</td>
						<td>
							<button class="btn btn-warning btn-sm" title="Edit Recipe" data-toggle="modal" data-target="#modal-Input">
								<i class="glyphicon glyphicon-pencil"></i>
							</button>
							<button class="btn btn-danger btn-sm" title="Delete Recipe" data-toggle="modal" data-target="#modal-delete">
								<i class="glyphicon glyphicon-trash"></i>
							</button>
						</td>
					</tr>
                </tbody>
				<tfoot>                 
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
	  </div>
      <!-- Main row -->
      
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->