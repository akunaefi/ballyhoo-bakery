<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Order
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.html"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Order</li>
		<li class="active">List Order</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">New Order</h3>
        </div>
        <div class="box-body">
          <form action="<?=base_url()?>buatlaporan/save" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label>Customer name</label>
              <select class="form-control" name="customer" id="customer" required="required">
                <option></option>
                <option>Gusti Syailendra</option>
                <option>Andika</option>
                <option>Anik</option>
              </select>
            </div>
            <div class="form-group">
              <label>Jenis Roti</label>
              <select class="form-control" name="LPR_URGENSI" id="LPR_URGENSI" required="required">
                <option></option>
                <option value="LOW">Kue Bolu</option>
                <option value="MEDIUM">Roti Sosis</option>
                <option value="HIGH">Roti Abon</option>
              </select>
            </div>
            <div class="form-group">
              <label>Diambil Tanggal</label>
              <input type="text" class="form-control" placeholder="Masukkan Subjek" name="LPR_SUBJECT" id="LPR_SUBJECT" required>
            </div>
            <div class="form-group">
              <label>Catatan Pembelian</label>
              <input type="text" class="form-control" placeholder="Masukkan Subjek" name="LPR_SUBJECT" id="LPR_SUBJECT" required>
            </div>
            <button type="submit" class="btn btn-warning" name="button"><i class="glyphicon glyphicon-plus"></i> Tambah</button>
          </form>
        </div>

        <div class="box-body">
          <div class="form-group">
            <label>Total Harga</label>
            <div class="callout callout-danger">
              <h4>Rp. 1.850.000,-</h4>
            </div>
          </div>
        </div>

        <div class="box-body">
          <table class="table table-condensed">
            <thead>
              <tr>
                <th>NO</th>
                <th>Jenis Roti</th>
                <th>QTY</th>
                <th>Harga @</th>
                <th>Jumlah</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1.</td>
                <td>Roti Bolu</td>
                <td>10</td>
                <td>Rp. 100.000,-</td>
                <td>Rp. 1.000.000,-</td>
              </tr>
              <tr>
                <td>2.</td>
                <td>Kue Tart Besar</td>
                <td>1</td>
                <td>Rp. 850.000,-</td>
                <td>Rp. 850.000,-</td>
              </tr>
            </tbody>
          </table>
        </div>

        <div class="box-body">
          <button type="submit" class="btn btn-warning col-lg-12 btn-lg" name="button"><i class="glyphicon glyphicon-check"></i> Bayar</button>
        </div>

      </div>
	 
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->