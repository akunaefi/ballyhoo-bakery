<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Ingredients
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Ingredients</li>
		<li class="active">List Ingredients</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	  <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
			<div id="modal-tambah" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Ingredients</h4>
						</div>
						<div class="modal-body">
							<form action="#" method="post" enctype="multipart/form-data">
								<div class="form-group">	
									<label>Ingredients Name</label>
									<input type="text" class="form-control" placeholder="Masukkan Nama Bahan" name="LPR_SUBJECT" id="LPR_SUBJECT" required>
								</div>
								<div class="form-group">	
									<label>Ingredients Type</label>
									<input type="text" class="form-control" placeholder="Masukkan Jenis Bahan" name="LPR_SUBJECT" id="LPR_SUBJECT" required>
								</div>
								<div class="form-group">
									<label>Ingredients Quantity</label>
									<table>
										<td style="width:50%">
											<input type="text" class="form-control" placeholder="Masukkan Jumlah Bahan" name="LPR_SUBJECT" id="LPR_SUBJECT" required>
										</td>
										<td style="width:50%">
											<input style="margin-left:10px" type="text" class="form-control" placeholder="Masukan Satuan Bahan" name="LPR_SUBJECT" id="LPR_SUBJECT" required>
										</td>
									</table>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary"><i class="icon-checkmark-circle2"></i> Simpan</button>
						</div>
					</div>
				</div>
			</div>
            <div class="box-header">
              <h3 class="box-title">List Ingredients</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
			  <div class="modal fade" id="modal-delete">
				<div class="modal-dialog">
					<div class="modal-content">
						<!-- Modal Header -->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Delete</h4>
						</div>
						<!-- Modal body -->
						<div class="modal-body">
							Are you sure want to delete "Margarine" ?
						</div>
						<!-- Modal footer -->
						<div class="modal-footer">
							<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">Delete</button>
						</div>
					</div>
				</div>
			  </div>
			  <div class="form-group">
				<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-tambah"><i class="glyphicon glyphicon-plus"></i> Add </button>
			  </div>
              <table id="example1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Ingredients ID</th>
						<th>Ingredients Name</th>
						<th>Ingredients Type</th>
						<th>Ingredients Quantity</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>uh78</td>
						<td>Margarine</td>
						<td>Pengembang</td>
						<td>30 Kg</td>
						<td>
							<button class="btn btn-warning btn-sm" title="Edit Recipe" data-toggle="modal" data-target="#modal-tambah">
								<i class="glyphicon glyphicon-pencil"></i>
							</button>
							<button class="btn btn-danger btn-sm" title="Delete Recipe" data-toggle="modal" data-target="#modal-delete">
								<i class="glyphicon glyphicon-trash"></i>
							</button>
						</td>
					</tr>
                </tbody>
				<tfoot>                 
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
	  </div>
      <!-- Main row -->
      
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->