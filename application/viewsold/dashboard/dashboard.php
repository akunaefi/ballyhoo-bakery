  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150</h3>

              <p>New Order</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>100</h3>

              <p>New Customer</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>

              <p>Delivery Order</p>
            </div>
            <div class="icon">
              <i class="ion ion-paper-airplane"></i>
            </div>
            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>65</h3>

              <p>Delivered Order</p>
            </div>
            <div class="icon">
              <i class="ion ion-paper-airplane"></i>
            </div>
            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
    <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <div class="box-header">
        
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-hover">
                <tr>
                  <th>Order ID</th>
                  <th>Customer Name</th>
                  <th>Order Details</th>
                  <th>Waktu</th>
          <th>Status</th>
                </tr>
                <tr>
                  <td>183</td>
                  <td>John Doe</td>
                  <td>Roti Keju, Roti Sosis</td>
          <td>08.00</td>
                  <td><span class="btn label-danger btn-sm">WO</span></td>
                </tr>
                <tr>
                  <td>219</td>
                  <td>Alexander Pierce</td>
                  <td>Donat</td>
          <td>10.30</td>
                  <td><span class="btsn label-warning btn-sm">Production</span></td>
                </tr>
                <tr>
                  <td>657</td>
                  <td>Bob Doe</td>
                  <td>Roti Tawar</td>
          <td>12.10</td>
                  <td><span class="btn label-info btn-sm">Waiting To Deliver</span></td>
                </tr>
                <tr>
                  <td>175</td>
                  <td>Mike Doe</td>
                  <td>Roti Kelapa</td>
          <td>14.08</td>
                  <td><span class="btn bg-blue btn-sm">Delivered</span></td>
                </tr>
        <tr>
                  <td>175</td>
                  <td>Mike Doe</td>
                  <td>Roti Kelapa</td>
          <td>14.10</td>
                  <td><span class="btn label-success btn-sm">Closed Order</span></td>
                </tr>
        <tr>
                  <td>175</td>
                  <td>Mike Doe</td>
                  <td>Roti Kelapa</td>
          <td>14.10</td>
                  <td><span class="btn label-success btn-sm">Closed Order</span></td>
                </tr>
        <tr>
                  <td>175</td>
                  <td>Mike Doe</td>
                  <td>Roti Kelapa</td>
          <td>14.10</td>
                  <td><span class="btn label-success btn-sm">Closed Order</span></td>
                </tr>
        <tr>
                  <td>175</td>
                  <td>Mike Doe</td>
                  <td>Roti Kelapa</td>
          <td>14.10</td>
                  <td><span class="btn label-success btn-sm">Closed Order</span></td>
                </tr>
        <tr>
                  <td>175</td>
                  <td>Mike Doe</td>
                  <td>Roti Kelapa</td>
          <td>14.10</td>
                  <td><span class="btn label-success btn-sm">Closed Order</span></td>
                </tr>
        <tr>
                  <td>175</td>
                  <td>Mike Doe</td>
                  <td>Roti Kelapa</td>
          <td>14.10</td>
                  <td><span class="btn label-success btn-sm">Closed Order</span></td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
    </div>
      <!-- Main row -->
      
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
