/*
 Navicat Premium Data Transfer

 Source Server         : job
 Source Server Type    : MySQL
 Source Server Version : 100133
 Source Host           : localhost:3306
 Source Schema         : ballyhoo

 Target Server Type    : MySQL
 Target Server Version : 100133
 File Encoding         : 65001

 Date: 15/10/2018 14:14:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bahan
-- ----------------------------
DROP TABLE IF EXISTS `bahan`;
CREATE TABLE `bahan`  (
  `BHN_ID` int(11) NOT NULL AUTO_INCREMENT,
  `SP_ID` int(11) NULL DEFAULT NULL,
  `KB_ID` int(11) NULL DEFAULT NULL,
  `ST_ID` int(11) NULL DEFAULT NULL,
  `KR_ID` int(11) NULL DEFAULT NULL,
  `GD_ID` int(11) NULL DEFAULT NULL,
  `BHN_NAMA` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `BHN_HARGA` int(11) NOT NULL,
  `BHN_STOK` int(11) NOT NULL,
  `BHN_MINSTOK` int(11) NOT NULL,
  `BHN_MINORDER` int(11) NOT NULL,
  `BHN_EXP` date NOT NULL,
  `BHN_UPDATE` date NOT NULL,
  `BHN_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`BHN_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_11`(`KB_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_12`(`SP_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_13`(`KR_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_7`(`GD_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_9`(`ST_ID`) USING BTREE,
  CONSTRAINT `FK_RELATIONSHIP_11` FOREIGN KEY (`KB_ID`) REFERENCES `kat_bahan` (`KB_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_RELATIONSHIP_12` FOREIGN KEY (`SP_ID`) REFERENCES `supplier` (`SP_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_RELATIONSHIP_13` FOREIGN KEY (`KR_ID`) REFERENCES `karyawan` (`KR_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_RELATIONSHIP_7` FOREIGN KEY (`GD_ID`) REFERENCES `gudang` (`GD_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_RELATIONSHIP_9` FOREIGN KEY (`ST_ID`) REFERENCES `satuan` (`ST_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bahan
-- ----------------------------
INSERT INTO `bahan` VALUES (1, 1, 1, 1, NULL, 1, 'Tepung Tapioka', 3, 16000, 50, 500, '2018-11-10', '2018-10-15', 1);

-- ----------------------------
-- Table structure for bahan_keluar
-- ----------------------------
DROP TABLE IF EXISTS `bahan_keluar`;
CREATE TABLE `bahan_keluar`  (
  `BK_ID` int(11) NOT NULL AUTO_INCREMENT,
  `BHN_ID` int(11) NULL DEFAULT NULL,
  `GD_ID` int(11) NULL DEFAULT NULL,
  `BK_TGL` date NOT NULL,
  `BK_QTY` int(11) NOT NULL,
  `BK_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`BK_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_17`(`BHN_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_18`(`GD_ID`) USING BTREE,
  CONSTRAINT `FK_RELATIONSHIP_17` FOREIGN KEY (`BHN_ID`) REFERENCES `bahan` (`BHN_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_RELATIONSHIP_18` FOREIGN KEY (`GD_ID`) REFERENCES `gudang` (`GD_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bahan_keluar
-- ----------------------------
INSERT INTO `bahan_keluar` VALUES (1, 1, NULL, '2018-10-15', 2000, 1);
INSERT INTO `bahan_keluar` VALUES (2, 1, NULL, '2018-10-15', 500, 1);

-- ----------------------------
-- Table structure for bahan_masuk
-- ----------------------------
DROP TABLE IF EXISTS `bahan_masuk`;
CREATE TABLE `bahan_masuk`  (
  `BM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `BHN_ID` int(11) NULL DEFAULT NULL,
  `GD_ID` int(11) NULL DEFAULT NULL,
  `BM_TGL` date NOT NULL,
  `BM_QTY` int(11) NOT NULL,
  `BM_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`BM_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_15`(`BHN_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_16`(`GD_ID`) USING BTREE,
  CONSTRAINT `FK_RELATIONSHIP_15` FOREIGN KEY (`BHN_ID`) REFERENCES `bahan` (`BHN_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_RELATIONSHIP_16` FOREIGN KEY (`GD_ID`) REFERENCES `gudang` (`GD_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bahan_masuk
-- ----------------------------
INSERT INTO `bahan_masuk` VALUES (1, 1, NULL, '2018-10-15', 5000, 1);
INSERT INTO `bahan_masuk` VALUES (2, 1, NULL, '2018-10-15', 5000, 1);
INSERT INTO `bahan_masuk` VALUES (3, 1, NULL, '2018-10-15', 5000, 1);
INSERT INTO `bahan_masuk` VALUES (4, 1, NULL, '2018-10-15', 2500, 1);

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer`  (
  `CTM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CTM_NAMA` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `CTM_ALAMAT` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `CTM_TELP` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `CTM_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`CTM_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for det_pesanan
-- ----------------------------
DROP TABLE IF EXISTS `det_pesanan`;
CREATE TABLE `det_pesanan`  (
  `DPS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PSN_ID` int(11) NULL DEFAULT NULL,
  `PRD_ID` int(11) NULL DEFAULT NULL,
  `DPS_QTY` int(11) NOT NULL,
  `DPS_HARGA` int(11) NOT NULL,
  `DPS_SATUAN` smallint(6) NOT NULL,
  PRIMARY KEY (`DPS_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_1`(`PSN_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_5`(`PRD_ID`) USING BTREE,
  CONSTRAINT `FK_RELATIONSHIP_1` FOREIGN KEY (`PSN_ID`) REFERENCES `pesanan` (`PSN_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_RELATIONSHIP_5` FOREIGN KEY (`PRD_ID`) REFERENCES `produk` (`PRD_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for det_resep
-- ----------------------------
DROP TABLE IF EXISTS `det_resep`;
CREATE TABLE `det_resep`  (
  `DRS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `RSP_ID` int(11) NULL DEFAULT NULL,
  `DRS_QTY` int(11) NOT NULL,
  `DRS_YIELD` smallint(6) NOT NULL,
  `DRS_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`DRS_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_2`(`RSP_ID`) USING BTREE,
  CONSTRAINT `FK_RELATIONSHIP_2` FOREIGN KEY (`RSP_ID`) REFERENCES `resep` (`RSP_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for food_cost
-- ----------------------------
DROP TABLE IF EXISTS `food_cost`;
CREATE TABLE `food_cost`  (
  `FC_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FC_NAMA` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `FC_VALUE` int(11) NOT NULL,
  `FC_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`FC_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gudang
-- ----------------------------
DROP TABLE IF EXISTS `gudang`;
CREATE TABLE `gudang`  (
  `GD_ID` int(11) NOT NULL AUTO_INCREMENT,
  `JB_ID` int(11) NULL DEFAULT NULL,
  `GD_NAMA` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `GD_ALAMAT` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `GD_TELP` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `GD_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`GD_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_21`(`JB_ID`) USING BTREE,
  CONSTRAINT `FK_RELATIONSHIP_21` FOREIGN KEY (`JB_ID`) REFERENCES `jenis_barang` (`JB_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gudang
-- ----------------------------
INSERT INTO `gudang` VALUES (1, 1, 'Gudang Bahan Mentah', 'wrq', 'qwr', 1);

-- ----------------------------
-- Table structure for jenis_barang
-- ----------------------------
DROP TABLE IF EXISTS `jenis_barang`;
CREATE TABLE `jenis_barang`  (
  `JB_ID` int(11) NOT NULL AUTO_INCREMENT,
  `JB_NAMA` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `JB_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`JB_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of jenis_barang
-- ----------------------------
INSERT INTO `jenis_barang` VALUES (1, 'Roti/cake', 1);

-- ----------------------------
-- Table structure for karyawan
-- ----------------------------
DROP TABLE IF EXISTS `karyawan`;
CREATE TABLE `karyawan`  (
  `KR_ID` int(11) NOT NULL AUTO_INCREMENT,
  `KP_ID` int(11) NULL DEFAULT NULL,
  `KR_NAMA` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `KR_ALAMAT` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `KR_TELP` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `KR_USERNAME` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `KR_PASSWORD` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `KR_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`KR_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_19`(`KP_ID`) USING BTREE,
  CONSTRAINT `FK_RELATIONSHIP_19` FOREIGN KEY (`KP_ID`) REFERENCES `kry_posisi` (`KP_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for kat_bahan
-- ----------------------------
DROP TABLE IF EXISTS `kat_bahan`;
CREATE TABLE `kat_bahan`  (
  `KB_ID` int(11) NOT NULL AUTO_INCREMENT,
  `JB_ID` int(11) NULL DEFAULT NULL,
  `KB_NAMA` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `KB_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`KB_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_10`(`JB_ID`) USING BTREE,
  CONSTRAINT `FK_RELATIONSHIP_10` FOREIGN KEY (`JB_ID`) REFERENCES `jenis_barang` (`JB_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kat_bahan
-- ----------------------------
INSERT INTO `kat_bahan` VALUES (1, 1, 'Cake', 1);

-- ----------------------------
-- Table structure for kat_produk
-- ----------------------------
DROP TABLE IF EXISTS `kat_produk`;
CREATE TABLE `kat_produk`  (
  `KPR_ID` int(11) NOT NULL AUTO_INCREMENT,
  `KPR_NAMA` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `KPR_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`KPR_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for kat_resep
-- ----------------------------
DROP TABLE IF EXISTS `kat_resep`;
CREATE TABLE `kat_resep`  (
  `KRS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `KRS_NAMA` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `KRS_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`KRS_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kat_resep
-- ----------------------------
INSERT INTO `kat_resep` VALUES (1, 'Cake', 1);

-- ----------------------------
-- Table structure for kry_posisi
-- ----------------------------
DROP TABLE IF EXISTS `kry_posisi`;
CREATE TABLE `kry_posisi`  (
  `KP_ID` int(11) NOT NULL AUTO_INCREMENT,
  `KP_KODE` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `KP_NAMA` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `KP_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`KP_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for multi_satuan
-- ----------------------------
DROP TABLE IF EXISTS `multi_satuan`;
CREATE TABLE `multi_satuan`  (
  `MS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `MS_SATUAN1` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `MS_VALUE1` int(11) NOT NULL,
  `MS_SATUAN2` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `MS_VALUE2` int(11) NOT NULL,
  `MS_STATUS` smallint(6) NULL DEFAULT NULL,
  PRIMARY KEY (`MS_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for pesanan
-- ----------------------------
DROP TABLE IF EXISTS `pesanan`;
CREATE TABLE `pesanan`  (
  `PSN_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CTM_ID` int(11) NULL DEFAULT NULL,
  `KR_ID` int(11) NULL DEFAULT NULL,
  `PSN_TANGGAL` date NOT NULL,
  `PSN_AMBIL` date NOT NULL,
  `PSN_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`PSN_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_14`(`KR_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_20`(`CTM_ID`) USING BTREE,
  CONSTRAINT `FK_RELATIONSHIP_14` FOREIGN KEY (`KR_ID`) REFERENCES `karyawan` (`KR_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_RELATIONSHIP_20` FOREIGN KEY (`CTM_ID`) REFERENCES `customer` (`CTM_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk`  (
  `PRD_ID` int(11) NOT NULL AUTO_INCREMENT,
  `KPR_ID` int(11) NULL DEFAULT NULL,
  `GD_ID` int(11) NULL DEFAULT NULL,
  `PRD_NAMA` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `PRD_HARGA` int(11) NOT NULL,
  `PRD_STOK` int(11) NOT NULL,
  `PRD_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`PRD_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_4`(`KPR_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_6`(`GD_ID`) USING BTREE,
  CONSTRAINT `FK_RELATIONSHIP_4` FOREIGN KEY (`KPR_ID`) REFERENCES `kat_produk` (`KPR_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_RELATIONSHIP_6` FOREIGN KEY (`GD_ID`) REFERENCES `gudang` (`GD_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for resep
-- ----------------------------
DROP TABLE IF EXISTS `resep`;
CREATE TABLE `resep`  (
  `RSP_ID` int(11) NOT NULL AUTO_INCREMENT,
  `KRS_ID` int(11) NULL DEFAULT NULL,
  `RSP_NAMA` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `RSP_JUMLAH` int(11) NOT NULL,
  `RSP_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`RSP_ID`) USING BTREE,
  INDEX `FK_RELATIONSHIP_3`(`KRS_ID`) USING BTREE,
  CONSTRAINT `FK_RELATIONSHIP_3` FOREIGN KEY (`KRS_ID`) REFERENCES `kat_resep` (`KRS_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for satuan
-- ----------------------------
DROP TABLE IF EXISTS `satuan`;
CREATE TABLE `satuan`  (
  `ST_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ST_KODE` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ST_NAMA` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ST_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`ST_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of satuan
-- ----------------------------
INSERT INTO `satuan` VALUES (1, 'Kg', 'Kilogram', 1);

-- ----------------------------
-- Table structure for supplier
-- ----------------------------
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier`  (
  `SP_ID` int(11) NOT NULL AUTO_INCREMENT,
  `SP_NAMA` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `SP_ALAMAT` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `SP_TELP` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `SP_STATUS` smallint(6) NOT NULL,
  PRIMARY KEY (`SP_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of supplier
-- ----------------------------
INSERT INTO `supplier` VALUES (1, 'PT. Indah Sejahtera', 'sdsda', '21ewqe', 1);

SET FOREIGN_KEY_CHECKS = 1;
