// This a javascript for edit table in data folder
// This is a update operation for the customer's table

$(document).on('click', '.editcustomer', function(){  
  var CTM_ID = $(this).attr("id");
  var url = base_url + 'data/customer/get_by_id';
  $.ajax({  
    url : url,  
    method:"POST",  
    data:{CTM_ID:CTM_ID},  
    dataType:"json",
    success:function(data){               
      $('#modalInput').modal('show');
      $('.modal-title').text("Edit Customer");
      $('#ID').val(data.CTM_ID);
      $('#CTM_NAMA').val(data.CTM_NAMA);
      $('#CTM_ALAMAT').val(data.CTM_ALAMAT);
      $('#CTM_TELP').val(data.CTM_TELP);
    }
  });
});

// This is a update operation for the employee's table

$(document).on('click', '.editemp', function(){  
  var KR_ID = $(this).attr("id");
  var url = base_url + 'data/karyawan/get_by_id';
  $.ajax({  
    url : url,  
    method:"POST",  
    data:{KR_ID:KR_ID},
    dataType:"json",
    success:function(data){               
      $('#modalInput').modal('show');
      $('.modal-title').text("Edit Karyawan");
      $('.hilang').show();
      $('#ID').val(data.KR_ID);
      $('#KR_NAMA').val(data.KR_NAMA);
      $('#KR_ALAMAT').val(data.KR_ALAMAT);
      $('#KR_TELP').val(data.KR_TELP);
      $('#KR_USERNAME').val(data.KR_USERNAME);
      $('#KR_PASSWORD').val(data.KR_PASSWORD);
      $('#KP_ID').val(data.KP_ID).change();
    }
  });
});

// This is a update operation for the users's table

$(document).on('click', '.editsupp', function(){  
  var SP_ID = $(this).attr("id");
  var url = base_url + 'data/supplier/get_by_id';
  $.ajax({  
    url : url,  
    method:"POST",  
    data:{SP_ID:SP_ID},
    dataType:"json",
    success:function(data){               
       $('#modalInput').modal('show');
      $('.modal-title').text("Edit Supplier");
      $('#ID').val(data.SP_ID);
      $('#SP_NAMA').val(data.SP_NAMA);
      $('#SP_ALAMAT').val(data.SP_ALAMAT);
      $('#SP_TELP').val(data.SP_TELP);
    }
  });
});

// This is a update operation for the gudang's table

$(document).on('click', '.editgd', function(){  
  var GD_ID = $(this).attr("id");
  var url = base_url + 'data/gudang/get_by_id';
  $.ajax({  
    url : url,  
    method:"POST",  
    data:{GD_ID:GD_ID},
    dataType:"json",
    success:function(data){               
      $('#modalInput').modal('show');
      $('.modal-title').text("Edit Gudang");
      $('#ID').val(data.GD_ID);
      $('#GD_ID').val(data.GD_ID);
      $('#GD_NAMA').val(data.GD_NAMA);
      $('#GD_ALAMAT').val(data.GD_ALAMAT);
      $('#GD_TELP').val(data.GD_TELP);
      $('#JB_ID').val(data.JB_ID).change();
    }
  });
});

// This is a update operation for the ingredient's table

$(document).on('click', '.editbhn', function(){  
  var BHN_ID = $(this).attr("id");
  var url = base_url + 'data/bahan/get_by_id';
  $.ajax({  
    url : url,  
    method:"POST",  
    data:{BHN_ID:BHN_ID},  
    dataType:"json",
    success:function(data){               
      $('#modalInput').modal('show');
      $('.modal-title').text("Edit Bahan");
      $('#ID').val(data.BHN_ID);
      $('#BHN_ID').val(data.BHN_ID);
      $('#BHN_NAMA').val(data.BHN_NAMA);
      $('#BHN_HARGA').val(data.BHN_HARGA);
      $('#BHN_STOK').val(data.BHN_STOK);
      $('#BHN_MINSTOK').val(data.BHN_MINSTOK);
      $('#BHN_MINORDER').val(data.BHN_MINORDER);
      $('#BHN_EXP').val(data.BHN_EXP);
      $('#BHN_UPDATE').val(data.BHN_UPDATE);
      $('#BHN_STATUS').val(data.BHN_STATUS);
      $('#SP_ID').val(data.SP_ID).change();
      $('#KB_ID').val(data.KB_ID).change();
      $('#ST_ID').val(data.ST_ID).change();
      $('#KR_ID').val(data.KR_ID).change();
      $('#GD_ID').val(data.GD_ID).change();
    }
  });
});

var stok = '';

$(document).on('click', '.detail', function(){  
  var BHN_ID = $(this).attr("id");
  var url = base_url + 'data/bahan/get_by_id';
  $.ajax({  
    url : url,  
    method:"POST",  
    data:{BHN_ID:BHN_ID},  
    dataType:"json",
    success:function(data){
      $('#tabel').hide();
      $('#dispdetail').show();
      $('#DBHN_NAMA').val(data.BHN_NAMA);
      $('#DBHN_HARGA').val(data.BHN_HARGA);
      $('#DBHN_HARGA').mask('0.000.000.000', {reverse: true});
      $('#DBHN_STOK').val(data.BHN_STOK + ' ' + data.ST_NAMA);
      $('#DBHN_MINSTOK').val(data.BHN_MINSTOK + ' ' + data.ST_NAMA);
      $('#DBHN_MINORDER').val(data.BHN_MINORDER + ' ' + data.ST_NAMA);
      $('#DBHN_EXP').val(data.BHN_EXP);
      $('#DBHN_STATUS').val(data.BHN_STATUS);
      $('#DKB_NAMA').val(data.KB_NAMA);
      $('#DSP_NAMA').val(data.SP_NAMA);
      $('#DGD_NAMA').val(data.GD_NAMA + ' - ' + data.JB_NAMA);
      $('#DSP_ALAMAT').val(data.SP_ALAMAT);
      $('#DGD_ALAMAT').val(data.GD_ALAMAT);
      $('#DSP_TELP').val(data.SP_TELP);
      $('#DGD_TELP').val(data.GD_TELP);
      $('#stok').addClass(data.stok);
      stok = data.stok;
    }
  });
});

$(document).on('click', '.kembali', function(){
  $('#stok').removeClass(stok);
  $('#det_form')[0].reset();
  $('#tabel').show();
  $('#dispdetail').hide();  
});

// This is a update operation for the product's table

$(document).on('click', '.editprod', function(){  
  var PRD_ID = $(this).attr("id");
  var url = base_url + 'data/produk/get_by_id';
  $.ajax({  
    url : url,  
    method:"POST",  
    data:{PRD_ID:PRD_ID},  
    dataType:"json",
    success:function(data){               
      $('#modalInput').modal('show');
      $('.modal-title').text("Edit Produk");
      $('#ID').val(data.PRD_ID);
      $('#KPR_ID').val(data.KPR_ID).change();
      $('#GD_ID').val(data.GD_ID).change();
      $('#PRD_NAMA').val(data.PRD_NAMA);
      $('#PRD_HARGA').val(data.PRD_HARGA);
      $('#PRD_STOK').val(data.PRD_STOK);
    }
  });
});

$(document).on('click', '.barang', function(){
  $('#tabel').hide();
  $('#barang').show();
});

$(document).on('click', '.keluar', function(){
  $('#tabel').show();
  $('#barang').hide();
});

$(document).on('click', '.ambil', function(){  
  var BHN_ID = $(this).attr("id");
  var url = base_url + 'data/bahan/get_by_id';
  $.ajax({  
    url : url,  
    method:"POST",  
    data:{BHN_ID:BHN_ID},  
    dataType:"json",
    success:function(data){
      $('#modalInput').modal('show');
      $('.modal-title').text("Bahan Keluar");
      $('#BHN_ID').val(data.BHN_ID);
      $('#GD_ID').val(data.GD_ID);
      $('#BHN_NAMA').val(data.BHN_NAMA);
      $('#BHN_STOK').val(data.BHN_STOK);
      $('#SP_NAMA').val(data.SP_NAMA);
      $('#ST_NAMA').val(data.ST_NAMA);
      $('#ST_NAMA1').val(data.ST_NAMA);
      $('#GD_NAMA').val(data.GD_NAMA + ' - ' + data.JB_NAMA);
    }
  });
});