// This a javascrpt for CRUD

var base_url = 'http://localhost/ballyhoo-bakery/';

// This is a delete and insert operation for all

$(document).on('click', '.tambah', function(){
  $('#add_form')[0].reset();
  $('#ID').val('');
  $('.hilang').hide();
  $('#modalInput').modal('show');
});

function save(url){
  $.ajax({
    url : base_url + url,
    type : 'post',
    data: $('#add_form').serialize(),
    success : function(data)
    {
      $('#modalInput').modal('hide');
      location.reload();
    },
    error : function(res){
      show_message('Gagal',(res.responseText));
    }
  });
}

function konfirmasi(url){
  url : url;
  tanya = confirm("Anda yakin ingin menghapus data ini ?")
  if (tanya == true) return hapus(url);
}

function hapus(url){
  $.ajax({
    url : base_url + url,
    type : 'post',
    dataType : 'json',
    success : function(data)
    {
      if(data.status == 'ok')
      {
        alert("Data Terhapus");
        location.reload();
      }
    },
    error : function(res)
    {
      show_message('Gagal',(res.responseText));
      alert(url);
    }
  });
  //location.reload();
}

// This is a update operation for the kategori bahan's table

$(document).on('click', '.editkatbhn', function(){
  var KB_ID = $(this).attr("id");
  var url = base_url + 'kategori/bahan/get_by_id';
  $.ajax({  
    url : url,  
    method:"POST",  
    data:{KB_ID:KB_ID},
    dataType:"json",
    success:function(data){
      $('.hilang').show();
      $('#modalInput').modal('show');
      $('.modal-title').text("Edit Kategori Bahan");
      $('#ID').val(data.KB_ID);
      $('#KB_NAMA').val(data.KB_NAMA);
      $('#JB_ID').val(data.JB_ID).change();
    }
  });
});

// This is a update operation for the jenis barang's table

$(document).on('click', '.editjb', function(){
  var JB_ID = $(this).attr("id");
  var url = base_url + 'kategori/jenis_barang/get_by_id';
  $.ajax({  
    url : url,  
    method:"POST",  
    data:{JB_ID:JB_ID},
    dataType:"json",
    success:function(data){
      $('.hilang').show();
      $('#modalInput').modal('show');
      $('.modal-title').text("Edit Kategori Jenis Barang");
      $('#ID').val(data.JB_ID);
      $('#JB_ID').val(data.JB_ID);
      $('#JB_NAMA').val(data.JB_NAMA);
    }
  });
});

// This is a update operation for the kry_posisi's table

$(document).on('click', '.editpos', function(){  
  var KP_ID = $(this).attr("id");
  var url = base_url + 'kategori/posisi_karyawan/get_by_id';
  $.ajax({  
    url : url,  
    method:"POST",  
    data:{KP_ID:KP_ID},  
    dataType:"json",
    success:function(data){               
      $('#modalInput').modal('show');
      $('.modal-title').text("Edit Posisi Karyawan");
      $('#ID').val(data.KP_ID);
      $('#KP_NAMA').val(data.KP_NAMA);
      $('#KP_ID').val(data.KP_ID);
    }
  });
});

// This is a update operation for the foodcost's table

$(document).on('click', '.editcost', function(){  
  var FC_ID = $(this).attr("id");
  var url = base_url + 'kategori/food_cost/get_by_id';
  $.ajax({  
    url : url,  
    method:"POST",  
    data:{FC_ID:FC_ID},  
    dataType:"json",
    success:function(data){               
      $('#modalInput').modal('show');
      $('.modal-title').text("Edit Biaya Makanan");
      $('#ID').val(data.FC_ID);
      $('#FC_NAMA').val(data.FC_NAMA);
      $('#FC_VALUE').val(data.FC_VALUE);
    }
  });
});

// This is a update operation for the satuan's table

$(document).on('click', '.editsat', function(){  
  var ST_ID = $(this).attr("id");
  var url = base_url + 'kategori/satuan/get_by_id';
  $.ajax({  
    url : url,  
    method:"POST",  
    data:{ST_ID:ST_ID},  
    dataType:"json",
    success:function(data){               
      $('#modalInput').modal('show');
      $('.modal-title').text("Edit Satuan");
      $('#ID').val(data.ST_ID);
      $('#ST_KODE').val(data.ST_KODE);
      $('#ST_NAMA').val(data.ST_NAMA);
    }
  });
});

// This is a update operation for the unit multi's table

$(document).on('click', '.editms', function(){

  var MS_ID = $(this).attr("id");
  var url = base_url + 'kategori/multi_satuan/get_by_id';
  $.ajax({  
    url : url,  
    method:"POST",  
    data:{MS_ID:MS_ID},  
    dataType:"json",
    success:function(data){               
      $('#modalInput').modal('show');
      $('.modal-title').text("Edit Multi Satuan");
      $('#ID').val(data.MS_ID);
      $('#MS_ID').val(data.MS_ID);
      $('#MS_VALUE1').val(data.MS_VALUE1);
      $('#MS_SATUAN1').val(data.MS_SATUAN1).change();
      $('#MS_VALUE2').val(data.MS_VALUE2);
      $('#MS_SATUAN2').val(data.MS_SATUAN2).change();
    }
  });
});


// This is a update operation for the product category's table

$(document).on('click', '.editkpr', function(){
  var KPR_ID = $(this).attr("id");
  var url = base_url + 'kategori/produk/get_by_id';
  $.ajax({  
    url : url,  
    method:"POST",  
    data:{KPR_ID:KPR_ID},
    dataType:"json",
    success:function(data){
      $('.hilang').show();
      $('#modalInput').modal('show');
      $('.modal-title').text("Edit Kategori Produk");
      $('#ID').val(data.KPR_ID);
      $('#KPR_ID').val(data.KPR_ID);
      $('#KPR_NAMA').val(data.KPR_NAMA);
    }
  });
});

// This is a update operation for the recipe category's table

$(document).on('click', '.editkrs', function(){
  var KRS_ID = $(this).attr("id");
  var url = base_url + 'kategori/resep/get_by_id';
  $.ajax({  
    url : url,  
    method:"POST",  
    data:{KRS_ID:KRS_ID},
    dataType:"json",
    success:function(data){
      $('.hilang').show();
      $('#modalInput').modal('show');
      $('.modal-title').text("Edit Multi Satuan");
      $('#ID').val(data.KRS_ID);
      $('#KRS_ID').val(data.KRS_ID);
      $('#KRS_NAMA').val(data.KRS_NAMA);
    }
  });
});